name: tlogger2
version: "2.0"
author: Karol Będkowski
about: Log your work.
global_settings:
  #    - subcommandrequiredelsehelp
  #    - argrequiredelsehelp
    - coloredhelp
    - versionlesssubcommands
args:
    - file:
        short: f
        long: file
        help: >
          Set a custom log file. Last file is used for writing into.
        multiple: true
        takes_value: true
        use_delimiter: true
        require_delimiter: true
        env: TLOGGER_FILE
        value_hint: FilePath
    - verbose:
        short: v
        long: verbose
        multiple: true
        takes_value: false
        help: Sets the level of verbosity

    - config:
        short: c
        long: config
        help: >
          Configuration file name; default for Linux:
          $XDG_CONFIG/tlogger2/config.yml or $HOME/.config/tlogger2/config.yml
        env: TLOGGER_CONF
        takes_value: true
        value_hint: FilePath

subcommands:
    - print:
        about: Load & print log file content.
        args:
          - begin:
              short: b
              long: begin
              help: select entries with date after given date
              takes_value: true
          - end:
              short: e
              long: end
              help: select entries with date before given date
              takes_value: true
          - contains:
              short: c
              long: contains
              help: select entries contains text
              takes_value: true
          - project:
              short: p
              long: project
              help: select entries by project
              takes_value: true
          - type:
              short: t
              long: type
              help: select entries by type
              takes_value: true
              use_delimiter: true
              require_delimiter: true
              multiple: true
              possible_values:
                  - t
                  - task
                  - m
                  - marker
                  - n
                  - note
                  - a
                  - any
          - output:
              short: o
              long: output
              help: "output file name; use `-` for stdout (default)"
              takes_value: true
          - output_format:
              short: O
              long: output-format
              help: "output format"
              takes_value: true
              possible_values:
                  - text
                  - csv
          - INPUT:
              multiple: true
              help: additional filters

    - report:
        about: >
          Create report that shows time spend on each project, task, etc.
        args:
          - begin:
              short: b
              long: begin
              help: select entries with date after given date
              takes_value: true
          - end:
              short: e
              long: end
              help: select entries with date before given date
              takes_value: true
          - contains:
              short: c
              long: contains
              help: select entries contains text
              takes_value: true
          - project:
              short: p
              long: project
              help: select entries by project
              multiple: true
              use_delimiter: true
              require_delimiter: true
              takes_value: true
          - type:
              short: t
              long: type
              help: select entries by type
              takes_value: true
              use_delimiter: true
              require_delimiter: true
              multiple: true
              possible_values:
                  - t
                  - task
                  - m
                  - marker
                  - n
                  - note
                  - a
                  - any
          - group:
              short: g
              long: group
              help: >
                group entries by: year,quarter,month,week,day,project;
                there can be up to 10 level of grouping
              takes_value: true
              use_delimiter: true
              require_delimiter: true
              multiple: true
          - max_depth:
              long: max-depth
              help: max depth of grouping
              takes_value: true
          - period:
              short: P
              long: period
              help: name of standard time period
              takes_value: true
              possible_values:
                  - "today"
                  - "yesterday"
                  - "past-week"
                  - "past-month"
                  - "past-year"
                  - "prev-week"
                  - "prev-month"
                  - "prev-quarter"
                  - "prev-year"
                  - "this-week"
                  - "this-month"
                  - "this-quarter"
                  - "this-year"
          - output_format:
              short: O
              long: output-format
              help: "output format"
              takes_value: true
              possible_values:
                  - text
                  - csv
          - output:
              short: o
              long: output
              help: "output file name; use `-` for stdout (default)"
              takes_value: true
          - flat:
              short: F
              long: flat
              help: flatten group hierarchy
              requires: group
          - INPUT:
              multiple: true
              help: additional filters

    - add:
        about: Add new entry to log file.
        args:
          - INPUT:
              help: content to add
              index: 1

    - end:
        about: Add entry with given duration (add also start marker).
        args:
          - duration:
              long: duration
              short: d
              help: 'duration in format hh:mm, hh:mm:ss, mm or [<x>d][<x>d][<y>h][<z>m][<q>s]'
              takes_value: true
              required: true
          - INPUT:
              help: content to add
              required: false
              index: 1

    - note:
        about: Add a note (comment)
        args:
          - INPUT:
              help: content to add
              required: true
              index: 1

    - start:
        about: "Add `start` marker"
        args:
          - INPUT:
              help: Content to add instead of `start`
              index: 1

    - projects:
        about: >
          Show used projects (in all or filtered entries)
        args:
          - begin:
              short: b
              long: begin
              help: select entries with date after given date
              takes_value: true
          - end:
              short: e
              long: end
              help: select entries with date before given date
              takes_value: true
          - contains:
              short: c
              long: contains
              help: select entries contains text
              takes_value: true
          - project:
              short: p
              long: project
              help: filter entries by project name
              takes_value: true
          - output:
              short: o
              long: output
              help: "output file name; use `-` for stdout (default)"
              takes_value: true
          - period:
              short: P
              long: period
              help: name of standard time period
              takes_value: true
              possible_values:
                  - "today"
                  - "yesterday"
                  - "past-week"
                  - "past-month"
                  - "past-year"
                  - "prev-week"
                  - "prev-month"
                  - "prev-quarter"
                  - "prev-year"
                  - "this-week"
                  - "this-month"
                  - "this-quarter"
                  - "this-year"

    - status:
        about: Print current status.

    - undo:
        about: Remove and show last entry from log file.

    - pomodoro:
        about: Start pomodoro timer.
        args:
          - duration:
              short: d
              long: duration
              help: task duration, default 15m
              takes_value: true

          - command:
              long: command
              help: command to run after time run out.
              takes_value: true

    - editor:
        alias: edit
        about: Start editor and edit log file.

    - archive:
        about: Move old entries into separate files.

    - generate:
        about: Generate shell completion routines.
        args:
          - shell:
              long: shell
              takes_value: true
              possible_values:
                  - "bash"
                  - "elvish"
                  - "fish"
                  - "powershell"
                  - "zsh"

#   - test:
#       about: App tests
#       args:
#         - command:
#             long: command
#             short: c
#             takes_value: true
