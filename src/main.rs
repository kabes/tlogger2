#![feature(once_cell)]
#![feature(trait_alias)]
#![feature(destructuring_assignment)]
//#![allow(dead_code)]
extern crate chrono;
extern crate chronoutil;
extern crate pretty_env_logger;
extern crate regex;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;
extern crate clap;
extern crate clap_generate;
extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate csv;
extern crate dirs_next;
extern crate serde;
//#[macro_use]
extern crate serde_derive;
extern crate shlex;

use clap::{load_yaml, App};
use clap_generate::generators::{Bash, Elvish, Fish, PowerShell, Zsh};
use clap_generate::{generate, Generator};

mod cli;
mod conf;
mod conv;
mod io;
mod register;
mod utils;

fn main() {
    pretty_env_logger::init();
    let yaml = load_yaml!("cli.yml");
    let app = App::from(yaml);
    let matches = app.get_matches();

    let conf = match conf::Config::load(matches.value_of("config")) {
        Ok(c) => c,
        Err(err) => {
            eprintln!("load config error: {}", err);
            return;
        }
    };
    debug!("conf: {:?}", &conf);

    match matches.occurrences_of("v") {
        1 => log::set_max_level(log::LevelFilter::Debug),
        2 => log::set_max_level(log::LevelFilter::Trace),
        _ => (),
    }

    trace!("matches: {:?}", &matches);

    let ctx = cli::CliCtx::new(&matches, &conf);

    let res = match matches.subcommand() {
        Some(("print", sub_m)) => cli::print(&ctx, sub_m),
        Some(("add", sub_m)) => cli::add(&ctx, sub_m),
        Some(("end", sub_m)) => cli::add_end(&ctx, sub_m),
        Some(("note", sub_m)) => cli::add_note(&ctx, sub_m),
        Some(("start", sub_m)) => cli::add_start(&ctx, sub_m),
        Some(("report", sub_m)) => cli::report(&ctx, sub_m),
        Some(("projects", sub_m)) => cli::print_projects(&ctx, sub_m),
        Some(("status", sub_m)) => cli::status(&ctx, sub_m),
        Some(("undo", sub_m)) => cli::undo(&ctx, sub_m),
        Some(("archive", sub_m)) => cli::archive(&ctx, sub_m),
        Some(("pomodoro", sub_m)) => cli::pomodoro(&ctx, sub_m),
        Some(("editor", sub_m)) => cli::editor(&ctx, sub_m),
        Some(("generate", sub_m)) => cli_print_completions(sub_m),
        Some(("test", sub_m)) => match sub_m.value_of("command") {
            Some("last_entry") => cli::last_entry(&ctx, sub_m),
            _ => {
                eprintln!("ERROR: unknown command\n");
                App::from(yaml).print_help().unwrap();
                return;
            }
        },
        _ => {
            eprintln!("ERROR: unknown command\n");
            App::from(yaml).print_help().unwrap();
            return;
        }
    };

    if let Err(err) = res {
        eprintln!("{}", err);
    }
}

fn print_completions<G: Generator>(app: &mut App) {
    use std::io;
    generate::<G, _>(app, app.get_name().to_string(), &mut io::stdout());
}

fn cli_print_completions(args: &clap::ArgMatches) -> Result<(), cli::CliError> {
    let yaml = load_yaml!("cli.yml");
    let mut app = App::from(yaml);

    if let Some(shell) = args.value_of("shell") {
        match shell {
            "bash" => print_completions::<Bash>(&mut app),
            "elvish" => print_completions::<Elvish>(&mut app),
            "fish" => print_completions::<Fish>(&mut app),
            "powershell" => print_completions::<PowerShell>(&mut app),
            "zsh" => print_completions::<Zsh>(&mut app),
            _ => panic!("Unknown generator"),
        }
    }

    Ok(())
}
