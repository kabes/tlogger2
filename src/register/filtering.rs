//
// filtering.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
#![allow(clippy::upper_case_acronyms)]

use chrono::*;
use pest::error::{Error, ErrorVariant};
use pest::Parser;
use std::fmt;

use super::entry::{Entry, EntryType};
use super::errors;
use crate::conv::parse_date::{parse_date_str, parse_range_name};

#[derive(Parser)]
#[grammar = "register/filter.pest"] // relative to src
struct FilterParser;

fn filter_begin_date(entry: &Entry, ndt: &NaiveDateTime) -> bool {
    //println!("{:?} {:?}", entry, ndt);
    if let Some(dt) = entry.date_time {
        dt >= *ndt
    } else {
        false
    }
}

fn filter_end_date(entry: &Entry, ndt: &NaiveDateTime) -> bool {
    if let Some(dt) = entry.date_time {
        dt < *ndt
    } else {
        false
    }
}

fn filter_contain(entry: &Entry, value: &str) -> bool {
    entry.content.contains(value)
}

fn filter_tag(entry: &Entry, tag: &str) -> bool {
    entry.content.contains(&tag)
}

fn filter_project(entry: &Entry, project: &str) -> bool {
    entry
        .project
        .as_ref()
        .map(|p| p.starts_with(project))
        .unwrap_or(false)
}

fn filter_type(entry: &Entry, e_type: &EntryType) -> bool {
    *e_type == EntryType::Any || *e_type == entry.entry_type
}

fn filter_compare(
    entry: &mut Entry,
    op: &CompareVerb,
    lhs: &str,
    rhs: &str,
) -> bool {
    // try tag
    let value = if let Some(value) = entry.tag(lhs) {
        value
    } else {
        return false;
    };

    let rhs = String::from(rhs);

    match op {
        CompareVerb::GEq => value >= rhs,
        CompareVerb::LEq => value <= rhs,
        CompareVerb::Less => value < rhs,
        CompareVerb::Greater => value > rhs,
        CompareVerb::Eq => value == rhs,
        CompareVerb::NEq => value != rhs,
    }
}

/// Filter object
#[derive(Debug)]
pub struct Filter {
    filter: Option<AstNode>,
    filter_str: Vec<String>,
}

impl Filter {
    /// Create new filter object
    pub fn new() -> Filter {
        Filter {
            filter: None,
            filter_str: Vec::new(),
        }
    }

    /// Check is entry match rules defined by filter
    pub fn is_match(&mut self, e: &mut Entry) -> bool {
        if let Some(filter) = &self.filter {
            filter.eval(e)
        } else {
            true
        }
    }

    /// Check is filter is empty
    pub fn is_empty(&self) -> bool {
        self.filter.is_none()
    }

    /// Get info about filter
    pub fn info(&self) -> String {
        if let Some(filter) = &self.filter {
            format!("{}", filter)
        } else {
            String::from("")
        }
    }

    /// Append string expression to filter
    pub fn push(&mut self, s: &str) {
        if s == "and" {
            return;
        }
        if !self.filter_str.is_empty() {
            self.filter_str.push(String::from("and"));
        }
        self.filter_str.push(format!("({})", s));
    }

    fn push_group(&mut self, s: String) {
        if !self.filter_str.is_empty() {
            self.filter_str.push(String::from("and"));
        }
        self.filter_str.push(String::from("("));
        self.filter_str.push(s);
        self.filter_str.push(String::from(")"));
    }

    /// Prepare filter - build ast
    pub fn prepare(&mut self) -> Result<(), errors::FilterError> {
        if self.filter_str.is_empty() {
            return Ok(());
        }
        let f = self.filter_str.join(" ");
        self.filter = Some(parse(&f).map_err(|err| {
            errors::FilterError::ArgumentError(err.to_string())
        })?);

        Ok(())
    }

    pub fn add_min_date(&mut self, d: NaiveDateTime) {
        self.push(&d.format("begin:%Y-%m-%d").to_string());
    }

    pub fn add_max_date(&mut self, d: NaiveDateTime) {
        self.push(&d.format("end:%Y-%m-%d").to_string());
    }

    pub fn add_projects(&mut self, projects: Vec<&str>) {
        self.push_group(
            projects
                .iter()
                .map(|p| String::from("proj:") + p)
                .collect::<Vec<String>>()
                .join(" and "),
        );
    }

    pub fn add_str(&mut self, s: &str) {
        self.push(s);
    }

    pub fn add_type(&mut self, types: Vec<&str>) {
        self.push_group(
            types
                .iter()
                .map(|t| String::from("type:") + t)
                .collect::<Vec<String>>()
                .join(" or "),
        );
    }

    pub fn add_range(&mut self, s: &str) {
        self.push(format!("range:{}", s).as_ref());
    }
}

impl fmt::Display for Filter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(filter) = &self.filter {
            return writeln!(f, "{}", filter);
        }
        Ok(())
    }
}

// helpers

fn parse_kind(
    pair: pest::iterators::Pair<Rule>,
) -> Result<EntryType, Error<Rule>> {
    let s = pair.as_str();
    let e_type = match s.to_lowercase().as_ref() {
        "t" | "task" => EntryType::Task,
        "m" | "marker" => EntryType::Marker,
        "n" | "note" => EntryType::Note,
        "a" | "any" => EntryType::Any,
        unknown => {
            return Err(Error::new_from_span(
                ErrorVariant::CustomError {
                    message: format!("invalid type: '{:?}'", unknown),
                },
                pair.as_span(),
            ));
        }
    };
    Ok(e_type)
}

fn parse_range_pair(
    pair: pest::iterators::Pair<Rule>,
) -> Result<AstNode, Error<Rule>> {
    let name = pair.as_str();
    let (db, de) = parse_range_name(name, None).map_err(|_| {
        Error::new_from_span(
            ErrorVariant::CustomError {
                message: format!("invalid range: '{:?}'", name),
            },
            pair.as_span(),
        )
    })?;
    Ok(AstNode::Range(String::from(name), db, de))
}

fn parse_date_pair(
    pair: pest::iterators::Pair<Rule>,
    end: bool,
) -> Result<NaiveDateTime, Error<Rule>> {
    let s = pair.as_str();
    parse_date_str(s, end).map_err(|_| {
        Error::new_from_span(
            ErrorVariant::CustomError {
                message: format!("invalid date format: '{:?}'", s),
            },
            pair.as_span(),
        )
    })
}

#[derive(PartialEq, Eq, Debug, Clone)]
enum UnaryVerb {
    Not,
}

#[derive(PartialEq, Eq, Debug, Clone)]
enum BinaryVerb {
    And,
    Or,
}

#[derive(PartialEq, Eq, Debug, Clone)]
enum CompareVerb {
    GEq,
    LEq,
    Eq,
    NEq,
    Less,
    Greater,
}

#[derive(PartialEq, Debug, Clone)]
enum AstNode {
    Ident(String),
    BeginDate(NaiveDateTime),
    EndDate(NaiveDateTime),
    Project(String),
    Tag(String),
    Type(EntryType),
    Range(String, NaiveDateTime, NaiveDateTime),
    BinaryOp {
        verb: BinaryVerb,
        lhs: Box<AstNode>,
        rhs: Box<AstNode>,
    },
    UnaryOp {
        verb: UnaryVerb,
        expr: Box<AstNode>,
    },
    CompareOp {
        verb: CompareVerb,
        lhs: String,
        rhs: String,
    },
}

impl AstNode {
    fn eval(&self, entry: &mut Entry) -> bool {
        match self {
            AstNode::Ident(s) => filter_contain(entry, s),
            AstNode::BinaryOp { verb, lhs, rhs } => match verb {
                BinaryVerb::And => lhs.eval(entry) && rhs.eval(entry),
                BinaryVerb::Or => lhs.eval(entry) || rhs.eval(entry),
            },
            AstNode::UnaryOp { verb, expr } => match verb {
                UnaryVerb::Not => !expr.eval(entry),
            },
            AstNode::BeginDate(d) => filter_begin_date(entry, d),
            AstNode::EndDate(d) => filter_end_date(entry, d),
            AstNode::Project(p) => filter_project(entry, p),
            AstNode::Tag(t) => filter_tag(entry, t),
            AstNode::Type(et) => filter_type(entry, et),
            AstNode::Range(_name, db, de) => {
                filter_begin_date(entry, db) && filter_end_date(entry, de)
            }
            AstNode::CompareOp { verb, lhs, rhs } => {
                filter_compare(entry, verb, lhs, rhs)
            }
        }
    }
}

impl fmt::Display for AstNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AstNode::Ident(s) => writeln!(f, " {} ", s),
            AstNode::BinaryOp { verb, lhs, rhs } => match verb {
                BinaryVerb::And => writeln!(f, " {} and {} ", lhs, rhs),
                BinaryVerb::Or => writeln!(f, " {} or {} ", lhs, rhs),
            },
            AstNode::UnaryOp { verb, expr } => match verb {
                UnaryVerb::Not => writeln!(f, " not {} ", expr),
            },
            AstNode::BeginDate(d) => writeln!(f, " begin:{} ", d),
            AstNode::EndDate(d) => writeln!(f, " end:{} ", d),
            AstNode::Project(p) => writeln!(f, " proj:{} ", p),
            AstNode::Tag(t) => writeln!(f, " tag:{} ", t),
            AstNode::Type(et) => writeln!(f, " type:{} ", et.to_string()),
            AstNode::Range(name, db, de) => {
                writeln!(f, " range:{} ({}-{})", name, db, de)
            }
            AstNode::CompareOp { verb, lhs, rhs } => {
                writeln!(f, " {} {:?} {}", lhs, verb, rhs)
            }
        }
    }
}

fn parse(source: &str) -> Result<AstNode, Error<Rule>> {
    let mut ast = vec![];

    let pairs = FilterParser::parse(Rule::filter, source)?;
    for pair in pairs {
        if pair.as_rule() == Rule::expr {
            ast.push(build_ast_from_expr(pair)?);
        }
    }

    if ast.len() == 1 {
        Ok(ast.pop().unwrap())
    } else {
        panic!("no ast")
    }
}

fn build_ast_from_expr(
    pair: pest::iterators::Pair<Rule>,
) -> Result<AstNode, Error<Rule>> {
    let res = match pair.as_rule() {
        Rule::expr => build_ast_from_expr(pair.into_inner().next().unwrap())?,
        Rule::unaryExpr => {
            let mut pair = pair.into_inner();
            let verb = pair.next().unwrap();
            let expr = pair.next().unwrap();
            let expr = build_ast_from_expr(expr)?;
            parse_unary_verb(verb, expr)
        }
        Rule::binaryExpr => {
            let mut pair = pair.into_inner();
            let lhspair = pair.next().unwrap();
            let lhs = build_ast_from_expr(lhspair)?;
            let verb = pair.next().unwrap();
            let rhspair = pair.next().unwrap();
            let rhs = build_ast_from_expr(rhspair)?;
            parse_binary_verb(verb, lhs, rhs)
        }
        Rule::compareExpr => {
            let mut pair = pair.into_inner();
            let lhspair = pair.next().unwrap();
            let lhs = lhspair.as_str();
            let verb = pair.next().unwrap();
            let rhspair = pair.next().unwrap();
            let rhs = rhspair.as_str();
            parse_compareop_verb(verb, lhs, rhs)
        }
        Rule::ident => AstNode::Ident(String::from(pair.as_str())),
        Rule::string_literal => {
            AstNode::Ident(String::from(pair.as_str()).trim_matches('"').into())
        }
        Rule::keyval => parse_keyval_verb(pair)?,
        unknown_expr => {
            return Err(Error::new_from_span(
                ErrorVariant::CustomError {
                    message: format!(
                        "Unexpected expression: {:?}",
                        unknown_expr
                    ),
                },
                pair.as_span(),
            ));
        }
    };
    Ok(res)
}

fn parse_keyval_verb(
    pair: pest::iterators::Pair<Rule>,
) -> Result<AstNode, Error<Rule>> {
    let mut pair = pair.into_inner();
    let keyp = pair.next().unwrap();
    let key = keyp.as_str();
    let val = pair.next().unwrap();
    let res = match key {
        "proj" => AstNode::Project(String::from(val.as_str())),
        "tag" => AstNode::Tag(String::from(val.as_str())),
        "begin" => AstNode::BeginDate(parse_date_pair(val, false)?),
        "end" => AstNode::EndDate(parse_date_pair(val, true)?),
        "type" => AstNode::Type(parse_kind(val)?),
        "range" => parse_range_pair(val)?,
        unknown_expr => {
            return Err(Error::new_from_span(
                ErrorVariant::CustomError {
                    message: format!("Unexpected key: {:?}", unknown_expr),
                },
                keyp.as_span(),
            ));
        }
    };
    Ok(res)
}

fn parse_binary_verb(
    pair: pest::iterators::Pair<Rule>,
    lhs: AstNode,
    rhs: AstNode,
) -> AstNode {
    AstNode::BinaryOp {
        lhs: Box::new(lhs),
        rhs: Box::new(rhs),
        verb: match pair.as_str() {
            "and" => BinaryVerb::And,
            "or" => BinaryVerb::Or,
            _ => panic!("Unexpected dyadic verb: {}", pair.as_str()),
        },
    }
}

fn parse_compareop_verb(
    pair: pest::iterators::Pair<Rule>,
    lhs: &str,
    rhs: &str,
) -> AstNode {
    AstNode::CompareOp {
        lhs: String::from(lhs),
        rhs: String::from(rhs),
        verb: match pair.as_str() {
            ">=" => CompareVerb::GEq,
            "<=" => CompareVerb::LEq,
            "<" => CompareVerb::Less,
            ">" => CompareVerb::Greater,
            "=" => CompareVerb::Eq,
            "!=" => CompareVerb::NEq,
            _ => panic!("Unexpected dyadic verb: {}", pair.as_str()),
        },
    }
}

fn parse_unary_verb(
    pair: pest::iterators::Pair<Rule>,
    expr: AstNode,
) -> AstNode {
    AstNode::UnaryOp {
        verb: match pair.as_str() {
            "not" => UnaryVerb::Not,
            _ => panic!("Unsupported monadic verb: {}", pair.as_str()),
        },
        expr: Box::new(expr),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;
    #[test]
    fn test_parse_filter1() {
        let inp = "(project1 or project2) and dalkdla or (ans and not sssss) and proj:test and tag:abc";
        assert!(parse(inp).is_ok());
        let inp = "(project1 or project2)";
        assert!(parse(inp).is_ok());
    }

    #[test]
    fn test_parse_kv() {
        let f = parse("proj:project1").unwrap();
        assert_eq!(f, AstNode::Project(String::from("project1")));
        let f = parse("begin:2020-01-02").unwrap();
        assert_eq!(
            f,
            AstNode::BeginDate(
                NaiveDateTime::parse_from_str(
                    "2020-01-02 0:0:0",
                    "%Y-%m-%d %H:%M:%S"
                )
                .unwrap()
            )
        );
        let f = parse("end:2020-01-02").unwrap();
        assert_eq!(
            f,
            AstNode::EndDate(
                NaiveDateTime::parse_from_str(
                    "2020-01-03 0:0:0",
                    "%Y-%m-%d %H:%M:%S"
                )
                .unwrap()
            )
        );
        let f = parse("tag:tagabc").unwrap();
        assert_eq!(f, AstNode::Tag(String::from("tagabc")));
    }

    #[test]
    fn test_parse_filter2() {
        let mut e1 =
            Entry::from_str("2021-04-23 12:34:56: project1: message").unwrap();
        let mut e2 =
            Entry::from_str("2020-02-23 01:23:45: project3: aaaaa").unwrap();

        let inp = "(project1 or project2)";
        let fltr = parse(inp).unwrap();
        assert!(fltr.eval(&mut e1));
        assert!(!fltr.eval(&mut e2));

        let fltr = parse("message or project2").unwrap();
        assert!(fltr.eval(&mut e1));
        assert!(!fltr.eval(&mut e2));

        let fltr = parse("message or project3").unwrap();
        assert!(fltr.eval(&mut e1));
        assert!(fltr.eval(&mut e2));

        let fltr = parse("begin:2021-01-01").unwrap();
        println!("{:?}", fltr);
        assert!(fltr.eval(&mut e1));
        assert!(!fltr.eval(&mut e2));

        let fltr = parse("begin:2020-01-01 and end:2021-01-01").unwrap();
        println!("{:?}", fltr);
        assert!(!fltr.eval(&mut e1));
        assert!(fltr.eval(&mut e2));
    }
}

/*
project1 and dalkdla or (ans and not sssss)
*/
