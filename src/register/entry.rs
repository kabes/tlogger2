//
// entry.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use chrono::Datelike;
use chrono::{NaiveDateTime, NaiveTime};
use regex::{Match, Regex};
use std::convert::TryFrom;
use std::fmt;
use std::lazy::OnceCell;
use std::str::FromStr;

use super::duration::Duration;
use super::errors::*;

/// Entry types
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum EntryType {
    /// Unknown entry type
    Unknown,
    /// Task (normal) entry
    Task,
    /// Marker - time is not measured
    Marker,
    /// Note - time is not measured
    Note,
    /// Comment in file
    Comment,
    /// Empty item
    None,
    /// Any item
    Any,
}

impl Default for EntryType {
    fn default() -> Self {
        EntryType::Unknown
    }
}

impl EntryType {
    pub fn is_time_marker(&self) -> bool {
        *self == EntryType::Task || *self == EntryType::Marker
    }

    pub fn has_duration(&self) -> bool {
        *self == EntryType::Task
    }
}

impl ToString for EntryType {
    fn to_string(&self) -> String {
        match self {
            EntryType::Task => "task",
            EntryType::Marker => "marker",
            EntryType::Note => "note",
            EntryType::Any => "any",
            _ => "unknown",
        }
        .into()
    }
}

/// Regular entry:
///
/// [# comments]
/// 2020-01-02 03:04[:05]: [<projects> ]<message>[ <flag>][ # <comment>]
///
/// projects = (<project_name>:)+
///
/// Marker
/// flag = "**"
/// i.e.:
/// 2020-01-02 03:04:05 start task **
/// 2020-01-02 03:04:05 project1: start**
///
/// Non-time counted entry (= note):
/// flag = "*"
/// i.e.:
/// 2020-01-02 03:04:05 project1: project note *
///
/// Plain note:
/// Entries without message and flags with only comment part, i.e.:
/// 2020-01-02 03:04:05 project1: # this is example note
///
/// Counter:
/// Every entry is counted as 1 unless exists tag "count:<value>"
///
/// Tags are in form:
/// key:value or +key or +key:value
/// in message or comment
///
#[derive(Debug, Eq, PartialEq, Default)]
pub struct Entry {
    /// Entry type
    pub entry_type: EntryType,
    /// date and time
    pub date_time: Option<NaiveDateTime>,
    /// entry text without date/time
    pub content: String,

    /// project defined in content
    pub project: Option<String>,
    /// comment in content
    pub comment: Option<String>,

    /// note is content without comments and projects
    pub note: String,

    /// start date for current entry (from previous marker)
    pub start_date_time: Option<NaiveDateTime>,

    /// entry duration
    pub duration: Option<Duration>,

    // internals
    /// tags parsed from whole entry (all comments, content etc)
    internal_tags: OnceCell<Vec<String>>,
}

fn parse_datetime(
    datetime_match: Option<Match>,
    line: &str,
) -> Result<Option<NaiveDateTime>, ParseError> {
    if let Some(datetime) = datetime_match {
        let dt = datetime.as_str().to_string();
        Ok(Some(
            NaiveDateTime::parse_from_str(&dt, "%Y-%m-%d %H:%M:%S")
                .or_else(|_| {
                    NaiveDateTime::parse_from_str(&dt, "%Y-%m-%d %H:%M")
                })
                .map_err(|e| ParseError::value_err(e, &dt, line))?,
        ))
    } else {
        Ok(None)
    }
}

lazy_static! {
    static ref ENTRY_RE: Regex = {
        Regex::new(
            r"^(?P<datetime>\d{4}-\d{2}-\d{2} \d{1,2}:\d{1,2}(:\d{1,2})?): +((?P<projects>([^\s:]+:)+) *)?(?P<note>.+?)(?P<comment> #.+)?$")
        .unwrap()
    };
}

impl FromStr for Entry {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let original = s;
        let s = original.trim();

        if s.is_empty() {
            return Ok(Entry::new(EntryType::None, original));
        }

        if s.starts_with('#') {
            return Ok(Entry::new(EntryType::Comment, original));
        }

        let cap = ENTRY_RE
            .captures(s)
            .ok_or_else(|| ParseError::from_str("invalid format"))?;

        let note = cap
            .name("note")
            .map(|n| n.as_str().to_owned())
            .unwrap_or_default();
        let entry_type = {
            if note.ends_with("**") {
                EntryType::Marker
            } else if note.ends_with('*') || note.is_empty() {
                EntryType::Note
            } else {
                EntryType::Task
            }
        };
        Ok(Entry {
            date_time: parse_datetime(cap.name("datetime"), s)?,
            project: cap.name("projects").map(|d| d.as_str().to_owned()),
            comment: cap.name("comment").map(|c| c.as_str().to_owned()),
            note,
            content: s.to_owned(),
            entry_type,
            ..Default::default()
        })
    }
}

impl TryFrom<&str> for Entry {
    type Error = ParseError;

    fn try_from(c: &str) -> Result<Self, Self::Error> {
        Entry::from_str(c)
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.entry_type {
            EntryType::Unknown => write!(f, "<unknown>"),
            EntryType::Task | EntryType::Marker | EntryType::Note => {
                if self.date_time.is_some() {
                    let _ = write!(f, "{} ", self.date_time.unwrap());
                }
                write!(f, "{}", self.content)
            }
            EntryType::Comment => write!(f, "{}", self.content),
            EntryType::None | EntryType::Any => write!(f, ""),
        }
    }
}

impl Entry {
    fn new(entry_type: EntryType, content: &str) -> Entry {
        Entry {
            entry_type,
            content: content.to_owned(),
            ..Default::default()
        }
    }

    pub fn message(&self) -> String {
        if let Some(ref p) = &self.project {
            p.to_string() + " " + &self.note
        } else {
            self.note.to_string()
        }
    }

    pub fn calc_duration_since(&mut self, prev_dt: NaiveDateTime) {
        if self.duration.is_some() {
            // do not recalculate
            if self.start_date_time.is_none() {
                let dur: chrono::Duration = self.duration.unwrap().into();
                self.start_date_time = Some(self.date_time.unwrap() - dur);
            }
            return;
        }
        if !self.entry_type.has_duration() || self.date_time.is_none() {
            // caluclate only for events with time
            return;
        }

        let start_date_time =
            if self.date_time.unwrap().date() == prev_dt.date() {
                // same day
                prev_dt
            } else {
                // other day; work day starts on 6:00
                NaiveDateTime::new(
                    self.date_time.unwrap().date(),
                    NaiveTime::from_hms(6, 0, 0),
                )
            };

        let duration = self
            .date_time
            .unwrap()
            .signed_duration_since(start_date_time)
            + chrono::Duration::minutes(1);
        self.duration = Some(duration.into());
        self.start_date_time = Some(start_date_time);
    }

    pub fn year(&self) -> Option<String> {
        self.date_time.map(|dt| dt.year().to_string())
    }
    pub fn month(&self) -> Option<String> {
        self.date_time.map(|dt| dt.format("%Y-%m").to_string())
    }
    pub fn day(&self) -> Option<String> {
        self.date_time.map(|dt| dt.format("%Y-%m-%d").to_string())
    }
    pub fn week(&self) -> Option<String> {
        self.date_time.map(|dt| dt.format("%Yw%V").to_string())
    }
    pub fn quarter(&self) -> Option<String> {
        self.date_time.map(|dt| {
            let year = dt.year();
            let quarte = (dt.month() / 4) + 1;
            format!("{}q{}", year, quarte)
        })
    }

    pub fn tags(&mut self) -> &Vec<String> {
        self.internal_tags.get_or_init(|| get_tags(&self.content))
    }

    pub fn tag(&mut self, key: &str) -> Option<String> {
        let tags = self.tags();
        let key = key.to_string() + ":";
        for tag in tags {
            if let Some(val) = tag.strip_prefix::<&str>(&key) {
                return Some(String::from(val));
            }
        }
        None
    }

    pub fn count(&self) -> i32 {
        get_counter(&self.content).unwrap_or(1)
    }

    pub fn project_as_vec(&self, levels: usize) -> Vec<String> {
        self.project.as_ref().map_or_else(
            || vec!["".to_string()],
            |p| {
                let project = p.to_string().trim_end_matches(':').to_string();
                if levels == 0 {
                    vec![project]
                } else {
                    let parts = project.splitn(levels + 1, ':');
                    let parts =
                        parts.map(|p| p.to_string()).collect::<Vec<String>>();
                    if parts.len() > levels {
                        parts[..levels].to_vec()
                    } else {
                        parts
                    }
                }
            },
        )
    }

    pub fn print(&self, details: bool) -> String {
        match self.entry_type {
            EntryType::Unknown => String::from("<unknown>"),
            EntryType::Task | EntryType::Marker | EntryType::Note => {
                let mut res = String::new();
                if let Some(date) = self.date_time {
                    res += format!("{} ", date).as_ref();
                }
                res += self.content.as_ref();
                if details {
                    if let Some(duration) = &self.duration {
                        res +=
                            format!("\n    ; duration: {}", duration).as_ref();
                    }
                }
                res
            }
            EntryType::Comment => String::from(&self.content),
            EntryType::None | EntryType::Any => String::from(""),
        }
    }
}

lazy_static! {
    static ref COUNTER_RE: Regex = Regex::new(r"\bcount:([+-]?\d+)\b").unwrap();
}

fn get_counter(s: &str) -> Option<i32> {
    if s.is_empty() {
        return None;
    }

    COUNTER_RE
        .captures(s)
        .and_then(|c| c.get(1).unwrap().as_str().parse::<i32>().ok())
}

lazy_static! {
    static ref TAG_RE1: Regex =
        Regex::new(r"^\+[a-zA-Z][a-zA-Z0-9_]{2,}(:\S+)?$").unwrap();
    static ref TAG_RE2: Regex =
        Regex::new(r"^[a-zA-Z][a-zA-Z0-9_]{2,}:\S+$").unwrap();
}

fn get_tags(content: &str) -> Vec<String> {
    let mut tags: Vec<String> = content
        .split_ascii_whitespace()
        .filter(|t| {
            !t.ends_with(':') && (TAG_RE1.is_match(t) || TAG_RE2.is_match(t))
        })
        .map(|t| t.trim_start_matches('+').to_string())
        .collect();

    if tags.is_empty() {
        return vec![];
    }

    tags.sort();
    tags.dedup();

    tags
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
    #[test]
    fn test_parse_entry() {
        let mut entry = Entry::from_str("2020-01-02 12:23: note").unwrap();
        assert_eq!(entry.entry_type, EntryType::Task);
        assert_eq!(entry.content, "2020-01-02 12:23: note".to_owned());
        assert_eq!(
            entry.date_time.unwrap(),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 1, 2),
                NaiveTime::from_hms(12, 23, 0)
            )
        );
        assert_eq!(entry.note, "note");
        assert_eq!(entry.project, None);
        assert_eq!(entry.comment, None);
        assert!(entry.tags().is_empty());

        let entry = Entry::from_str("2020-01-02 12:23:32: proj: note").unwrap();
        assert_eq!(entry.project.unwrap(), "proj:");

        let entry =
            Entry::from_str("2020-01-02 11:22:33: proj:proj2: note").unwrap();
        assert_eq!(entry.project.unwrap(), "proj:proj2:");

        let entry =
            Entry::from_str("2020-01-02 23:59:59: pro j:proj2: note").unwrap();
        assert_eq!(entry.project, None);
        assert_eq!(entry.note, "pro j:proj2: note".to_owned());

        let entry =
            Entry::from_str("2020-01-02 21:22:59: proj: note # comment")
                .unwrap();
        assert_eq!(entry.project.unwrap(), "proj:");
        assert_eq!(entry.note, "note".to_owned());
        assert_eq!(entry.comment.unwrap(), " # comment");

        let entry = Entry::from_str("2020-01-02 12:34: note").unwrap();
        assert_eq!(
            entry.date_time.unwrap(),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 1, 2),
                NaiveTime::from_hms(12, 34, 0)
            )
        );
        assert_eq!(entry.note, "note");

        let entry = Entry::from_str("2020-01-02 12:34:56: note").unwrap();
        assert_eq!(
            entry.date_time.unwrap(),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 1, 2),
                NaiveTime::from_hms(12, 34, 56)
            )
        );
        assert_eq!(entry.note, "note");

        let entry =
            Entry::from_str("2020-01-02 12:22: test:rs: count:1").unwrap();
        assert_eq!(entry.count(), 1i32);

        let entry = Entry::from_str(
            "2020-01-02 12:13: test:rs: daklsdk a count:+1 dasda",
        )
        .unwrap();
        assert_eq!(entry.count(), 1i32);

        let entry =
            Entry::from_str("2020-01-02 1:12:12: test:rs: 212 count:-1")
                .unwrap();
        assert_eq!(entry.count(), -1i32);
    }

    #[test]
    fn test_is_counter() {
        assert_eq!(get_counter(""), None);
        assert_eq!(get_counter("count:"), None);
        assert_eq!(get_counter("lal"), None);
        assert_eq!(get_counter("count:lal"), None);
        assert_eq!(get_counter("+1abc"), None);
        assert_eq!(get_counter("count:+1abc"), None);
        assert_eq!(get_counter("-1a"), None);
        assert_eq!(get_counter("count:-1a"), None);
        assert_eq!(get_counter("=1a"), None);
        assert_eq!(get_counter("count:=1a"), None);
        assert_eq!(get_counter(" =1a"), None);
        assert_eq!(get_counter(" count:=1a "), None);
        assert_eq!(get_counter("+10"), None);
        assert_eq!(get_counter(" count:+10"), Some(10i32));
        assert_eq!(get_counter("-100 "), None);
        assert_eq!(get_counter("count:-100 "), Some(-100i32));
        assert_eq!(
            get_counter("dasdlka dlka d count:-100  dnadn"),
            Some(-100i32)
        );
        assert_eq!(get_counter("-100  dnadn"), None);
        assert_eq!(get_counter("count:-100  dnadn"), Some(-100i32));
        assert_eq!(get_counter("-100 "), None);
        assert_eq!(get_counter("count d dalk count:-100 "), Some(-100i32));
        assert_eq!(get_counter("  +10"), None);
        assert_eq!(get_counter("  count:+10 lsdkal"), Some(10i32));
    }

    #[test]
    fn test_tags() {
        assert!(get_tags("",).is_empty());
        assert_eq!(get_tags(" +lal "), vec!["lal".to_owned()]);
        assert!(get_tags(" 1abc:").is_empty());
        assert_eq!(get_tags("+test"), vec!["test".to_owned()]);
        assert_eq!(get_tags(" +test:arg"), vec!["test:arg".to_owned()]);
        assert_eq!(
            get_tags(" test:arg +abc"),
            vec!["abc".to_owned(), "test:arg".to_owned()]
        );
        assert_eq!(
            get_tags(" test:arg +abc:12 +cda +cda"),
            vec!["abc:12".to_owned(), "cda".to_owned(), "test:arg".to_owned()]
        );
        assert_eq!(
            get_tags("2020-01-01 12:32 proj1:task1: simple entry with +tag # comment +tag2"),
            vec![ "tag".to_owned(), "tag2".to_owned() ]
        );
        assert_eq!(
            get_tags("2020-01-01 12:32 proj1:task1: simple entry with +tag # comment tag2:val"),
            vec![ "tag".to_owned(), "tag2:val".to_owned() ]
        );
    }
}
