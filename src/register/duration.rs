//
// duration.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use std::convert::TryFrom;
use std::fmt;
use std::ops::Add;
use thiserror::Error;

#[derive(Error, Debug, Clone)]
pub enum DurationParseError {
    #[error("parse field {field} error: {error}")]
    ParseFieldError { field: &'static str, error: String },

    #[error("{0}")]
    ParseError(String),
}

impl DurationParseError {
    fn new<E>(field: &'static str, err: &E) -> DurationParseError
    where
        E: std::error::Error,
    {
        DurationParseError::ParseFieldError {
            field,
            error: err.to_string(),
        }
    }
}

/// Duration represent time period (in sec)
#[derive(Debug, Default, Eq, PartialEq, Clone, Copy)]
pub struct Duration(u32);

impl From<u32> for Duration {
    fn from(val: u32) -> Self {
        Duration(val)
    }
}

impl From<chrono::Duration> for Duration {
    fn from(d: chrono::Duration) -> Self {
        Duration(d.num_seconds() as u32)
    }
}

fn get_val<'a>(
    d: &'a str,
    symbol: char,
    name: &'static str,
    prev_durration: u32,
    multpiler: u32,
) -> Result<(&'a str, u32), DurationParseError> {
    if let Some(idx) = d.find(symbol) {
        let value = d[..idx]
            .to_string()
            .trim()
            .parse::<u32>()
            .map_err(|e| DurationParseError::new(name, &e))?;
        Ok((d[idx + 1..].trim(), prev_durration + value * multpiler))
    } else {
        Ok((d, prev_durration))
    }
}

impl TryFrom<&str> for Duration {
    type Error = DurationParseError;

    /// Convert str to Duration.
    /// Accepted formats:
    /// [<x>d] [<y>h] [<z>m] [<q>s] - days, hours, minutes, seconds
    /// hh:mm:dd
    /// hh:mm
    /// mm
    fn try_from(d: &str) -> Result<Self, Self::Error> {
        // try as simple number = minutes
        let mut duration = 0;
        let mut d = d.trim();

        (d, duration) = get_val(d, 'd', "days", duration, 24 * 60 * 60)?;
        (d, duration) = get_val(d, 'h', "hours", duration, 60 * 60)?;
        (d, duration) = get_val(d, 'm', "minutes", duration, 60)?;
        (d, duration) = get_val(d, 's', "seconds", duration, 1)?;

        if d.is_empty() {
            return Ok(Duration(duration));
        }

        let groups: Vec<&str> = d.trim().split(':').collect();
        if groups.len() > 3 {
            return Err(DurationParseError::ParseError(
                "cant parse content".into(),
            ));
        }

        let groups: Result<Vec<u32>, DurationParseError> = groups
            .iter()
            .map(|g| {
                g.parse::<u32>().map_err(|e| {
                    DurationParseError::ParseError(format!(
                        "cant parse {} :{}",
                        &g, e
                    ))
                })
            })
            .collect();

        let groups = groups?;

        duration += match groups.len() {
            // 1 group -> minutes
            1 => groups[0] * 60,
            // 2 groups -> hours, minutes
            2 => groups[0] * 60 * 60 + groups[1] * 60,
            // 3 groups -> hours, minutes, seconds
            3 => groups[0] * 3600 + groups[1] * 60 + groups[2],
            _ => 0,
        };

        Ok(Duration(duration))
    }
}

impl From<Duration> for u32 {
    fn from(d: Duration) -> u32 {
        d.0
    }
}

impl From<&Duration> for u32 {
    fn from(d: &Duration) -> u32 {
        d.0
    }
}

impl From<Duration> for i64 {
    fn from(d: Duration) -> i64 {
        d.0 as i64
    }
}

impl From<Duration> for chrono::Duration {
    fn from(d: Duration) -> chrono::Duration {
        chrono::Duration::seconds(d.0 as i64)
    }
}

impl From<&Duration> for chrono::Duration {
    fn from(d: &Duration) -> chrono::Duration {
        chrono::Duration::seconds(d.0 as i64)
    }
}

impl fmt::Display for Duration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let duration = self.0;
        let days = (duration / 86400) as u32;
        let hours = ((duration % 86400) / 3600) as u32;
        let minutes = ((duration % 3600) / 60) as u32;
        let seconds = (duration % 60) as u32;
        if days > 0 {
            write!(f, "{}d {}:{:02}:{:02}", days, hours, minutes, seconds)
        } else if hours > 0 {
            write!(f, "{}:{:02}:{:02}", hours, minutes, seconds)
        } else {
            write!(f, "{}:{:02}", minutes, seconds)
        }
    }
}

impl Add for Duration {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl Duration {
    /// Create duration with 0 sec
    pub fn zero() -> Duration {
        Duration(0)
    }

    /// Check is duration 0 sec
    pub fn is_zero(&self) -> bool {
        self.0 == 0
    }

    /// Format duration for human
    pub fn human(&self) -> String {
        let duration = self.0;
        if duration == 0 {
            return String::from("0");
        }
        let mut result: Vec<String> = vec![];
        let days = (duration / 86400) as u32;
        if days > 0 {
            result.push(format!("{}d", days));
        }
        let hours = ((duration % 86400) / 3600) as u32;
        if hours > 0 {
            result.push(format!("{}h", hours));
        }
        let minutes = ((duration % 3600) / 60) as u32;
        if minutes > 0 {
            result.push(format!("{}m", minutes));
        }
        let seconds = (duration % 60) as u32;
        if seconds > 0 {
            result.push(format!("{}s", seconds));
        }

        result.join(" ")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_format_duration() {
        assert_eq!(format!("{}", Duration(0)), String::from("0:00"));
        assert_eq!(format!("{}", Duration(1)), String::from("0:01"));
        assert_eq!(format!("{}", Duration(59)), String::from("0:59"));
        assert_eq!(format!("{}", Duration(60)), String::from("1:00"));
        assert_eq!(format!("{}", Duration(61)), String::from("1:01"));
        assert_eq!(format!("{}", Duration(181)), String::from("3:01"));
        assert_eq!(format!("{}", Duration(753)), String::from("12:33"));
        assert_eq!(format!("{}", Duration(3599)), String::from("59:59"));
        assert_eq!(format!("{}", Duration(3600)), String::from("1:00:00"));
        assert_eq!(format!("{}", Duration(3601)), String::from("1:00:01"));
        assert_eq!(format!("{}", Duration(5591)), String::from("1:33:11"));
        assert_eq!(format!("{}", Duration(49500)), String::from("13:45:00"));
        assert_eq!(format!("{}", Duration(86400)), String::from("1d 0:00:00"));
        assert_eq!(format!("{}", Duration(105093)), String::from("1d 5:11:33"));
        assert_eq!(
            format!("{}", Duration(299159)),
            String::from("3d 11:05:59")
        );
    }

    #[test]
    fn test_add_duration() {
        assert_eq!(Duration(0) + Duration(60), Duration(60));
        assert_eq!(Duration(11) + Duration(71), Duration(82));
    }

    #[test]
    fn test_parse_duration() {
        assert_eq!(
            Duration::try_from("1d").unwrap(),
            Duration(1 * 24 * 60 * 60)
        );
        assert_eq!(Duration::try_from(" 3h ").unwrap(), Duration(3 * 60 * 60));

        assert_eq!(Duration::try_from("17m ").unwrap(), Duration(17 * 60));

        assert_eq!(
            Duration::try_from("2d 3h").unwrap(),
            Duration(2 * 24 * 60 * 60 + 3 * 60 * 60)
        );

        assert_eq!(
            Duration::try_from("4h 13m").unwrap(),
            Duration(4 * 60 * 60 + 13 * 60)
        );

        assert_eq!(
            Duration::try_from("1d4h 59s").unwrap(),
            Duration(1 * 24 * 60 * 60 + 4 * 60 * 60 + 59)
        );

        assert_eq!(Duration::try_from("0:00").unwrap(), Duration(0));
        assert_eq!(Duration::try_from("0:33").unwrap(), Duration(33 * 60));
        assert_eq!(Duration::try_from("1:00").unwrap(), Duration(60 * 60));
        assert_eq!(
            Duration::try_from("1:11").unwrap(),
            Duration(3600 + 11 * 60)
        );
        assert_eq!(
            Duration::try_from("1:12:32").unwrap(),
            Duration(3600 + 12 * 60 + 32)
        );
        assert_eq!(
            Duration::try_from("22:32:11").unwrap(),
            Duration(22 * 3600 + 32 * 60 + 11)
        );
        assert_eq!(
            Duration::try_from("1d 22:32:11").unwrap(),
            Duration(81131 + 86400)
        );
        assert_eq!(
            Duration::try_from("2d22:32:11").unwrap(),
            Duration(81131 + 172800)
        );
    }
}
