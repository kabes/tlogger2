//
// mod.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

mod duration;
mod entry;
mod filtering;
mod grouping;
mod parser;

pub mod errors;

pub use duration::Duration;
pub use entry::{Entry, EntryType};
pub use filtering::Filter;
pub use grouping::{Group, Grouper};
pub use parser::{Element, Parser};

use chrono::Local;
use std::collections::HashSet;
use std::fmt;

#[derive(Debug, Eq, PartialEq, Default)]
pub struct Project {
    pub name: String,
    pub counter: i32,
}

pub trait Register {
    fn append_entry(&mut self, e: Entry);
}

#[derive(Debug, Eq, PartialEq, Default)]
pub struct DefaultRegister {
    pub entries: Vec<Entry>,
    pub projects: HashSet<String>,
}

impl Register for DefaultRegister {
    fn append_entry(&mut self, e: Entry) {
        if let Some(project) = &e.project {
            self.projects.insert(String::from(project));
        }
        self.entries.push(e);
    }
}

impl DefaultRegister {
    pub fn projects(&self) -> Vec<String> {
        let mut names: Vec<String> =
            self.projects.iter().map(|p| p.to_owned()).collect();
        names.sort();
        names
    }

    pub fn duration_from_last(&self) -> Option<duration::Duration> {
        let now = Local::now().naive_local();
        for i in (0..self.entries.len()).rev() {
            let entry = self.entries.get(i).unwrap();
            if entry.entry_type.is_time_marker() {
                let edt = entry.date_time.unwrap();
                let duration = now - edt;
                return Some(duration::Duration::from(duration));
            }
        }
        None
    }
}

impl fmt::Display for DefaultRegister {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for entry in &self.entries {
            writeln!(f, "{}", entry)?;
        }
        Ok(())
    }
}

/// Register that only collect last entry messages
#[derive(Debug, Eq, PartialEq)]
pub struct LastMessagesRegister {
    max_messages: usize,
    pub messages: Vec<String>,
}

impl Register for LastMessagesRegister {
    fn append_entry(&mut self, e: Entry) {
        let message = e.message();
        if let Some(idx) = self.messages.iter().position(|e| *e == message) {
            self.messages.remove(idx);
        }
        self.messages.insert(0, message);
        if self.messages.len() > self.max_messages {
            self.messages.pop();
        }
    }
}

impl fmt::Display for LastMessagesRegister {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for entry in &self.messages {
            writeln!(f, "{}", entry)?;
        }
        Ok(())
    }
}

impl LastMessagesRegister {
    pub fn new(max_messages: usize) -> Self {
        LastMessagesRegister {
            max_messages,
            messages: Vec::with_capacity(max_messages + 1),
        }
    }
}
