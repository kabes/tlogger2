//
// errors.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use std::fmt::Display;
use thiserror::Error;

#[derive(Error, Debug)]
#[allow(clippy::enum_variant_names)]
pub enum ParseError {
    #[error("{0}")]
    GeneralError(String),

    #[error("can't load line '{line}'; value '{value:}' parse failed: {err:}")]
    ValueError {
        err: String,
        value: String,
        line: String,
    },

    #[error("error loading line '{line}' - it's before prevoius {prev_ts}")]
    WrongOrderError { line: String, prev_ts: String },
}

impl ParseError {
    pub fn from_str(s: &str) -> Self {
        ParseError::GeneralError(String::from(s))
    }

    pub fn value_err<T>(err: T, value: &str, line: &str) -> Self
    where
        T: std::error::Error + Display,
    {
        ParseError::ValueError {
            err: err.to_string(),
            value: value.to_string(),
            line: line.to_string(),
        }
    }
}

#[derive(Error, Debug)]
pub enum FilterError {
    #[error("argument: {0}")]
    ArgumentError(String),
}
