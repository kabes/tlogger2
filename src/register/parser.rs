//
// loader.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use super::errors::ParseError;
use super::*;
use chrono::NaiveDateTime;
use std::convert::TryFrom;
use std::fmt;

#[derive(Debug, Eq, PartialEq)]
pub enum Element {
    Entry(Entry),
    Unknown(String),
}

impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Element::Entry(e) => write!(f, "{}", e)?,
            Element::Unknown(s) => write!(f, "{}", s)?,
        }
        Ok(())
    }
}

pub struct Parser {
    // last ts of any type entry
    last_entry_ts: Option<NaiveDateTime>,
    // last ts of market/task enrty
    last_marker_ts: Option<NaiveDateTime>,
}

impl Parser {
    pub fn parse_line(&mut self, line: &str) -> Result<Element, ParseError> {
        let mut entry = Entry::try_from(line)?;

        if entry.entry_type == EntryType::Comment
            || entry.entry_type == EntryType::None
        {
            return Ok(Element::Entry(entry));
        }

        if entry.entry_type == EntryType::Unknown {
            return Ok(Element::Unknown(String::from(line)));
        }

        // check dates
        if let Some(ets) = entry.date_time {
            // check is date is after prev entry
            if let Some(pts) = self.last_entry_ts {
                if pts > ets {
                    return Err(ParseError::WrongOrderError {
                        line: line.to_string(),
                        prev_ts: pts.to_string(),
                    });
                }
            }
        } else {
            // for entries without date set date to last entry
            entry.date_time = self.last_entry_ts;
        }

        // calculate duration
        if entry.entry_type.has_duration() {
            if let Some(ts) = self.last_marker_ts {
                entry.calc_duration_since(ts);
            }
        }

        self.last_entry_ts = entry.date_time;
        if entry.entry_type.is_time_marker() {
            self.last_marker_ts = entry.date_time
        }
        Ok(Element::Entry(entry))
    }

    pub fn new() -> Parser {
        Parser {
            last_marker_ts: None,
            last_entry_ts: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

    #[test]
    fn test_parse_line_simple() {
        let mut parser = Parser::new();

        let line = "2021-01-01 10:15: start **";
        let element = parser.parse_line(line).unwrap();
        if let Element::Entry(entry) = element {
            assert_eq!(entry.entry_type, EntryType::Marker);
            assert_eq!(entry.note, String::from("start **"));
            assert_eq!(
                entry.date_time.unwrap(),
                NaiveDateTime::new(
                    NaiveDate::from_ymd(2021, 1, 1),
                    NaiveTime::from_hms(10, 15, 0)
                )
            );
        } else {
            panic!("wrong entry type: {:?}", element);
        }
    }

    #[test]
    fn test_parse_none_comment() {
        let mut parser = Parser::new();

        let entry = parser.parse_line("").unwrap();
        assert!(matches!(entry, Element::Entry(_)));
        if let Element::Entry(entry) = entry {
            assert_eq!(entry.entry_type, EntryType::None);
        }

        let entry = parser.parse_line("          ").unwrap();
        assert!(matches!(entry, Element::Entry(_)));
        if let Element::Entry(entry) = entry {
            assert_eq!(entry.entry_type, EntryType::None);
        }

        let line = "# 2021-01-01 10:15: start **";
        let entry = parser.parse_line(line).unwrap();
        assert!(matches!(entry, Element::Entry(_)));
        if let Element::Entry(entry) = entry {
            assert_eq!(entry.entry_type, EntryType::Comment);
            assert_eq!(entry.content, String::from(line));
        }

        let line = "       # 2021-01-01 10:15: start **";
        let entry = parser.parse_line(line).unwrap();
        assert!(matches!(entry, Element::Entry(_)));
        if let Element::Entry(entry) = entry {
            assert_eq!(entry.entry_type, EntryType::Comment);
            assert_eq!(entry.content, String::from(line));
        }
    }
}
