//
// grouping.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use std::collections::HashMap;
use std::fmt;
use std::str::FromStr;

use super::duration::Duration;
use super::entry::{Entry, EntryType};
use super::errors::FilterError;
use super::Register;

#[derive(Debug, Eq, PartialEq)]
enum GroupKey {
    Year,
    Month,
    Quarter,
    Week,
    Day,
    /// Grouping by project may use subprojects to certain level
    Project(usize),
    Note,
    Message,
}

impl FromStr for GroupKey {
    type Err = FilterError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(levels) = s.strip_prefix("project") {
            if levels.is_empty() {
                return Ok(GroupKey::Project(0));
            }
            return levels
                .parse::<usize>()
                .map(GroupKey::Project)
                .map_err(|_| FilterError::ArgumentError(s.to_string()));
        }

        match s {
            "year" => Ok(GroupKey::Year),
            "month" => Ok(GroupKey::Month),
            "quarter" => Ok(GroupKey::Quarter),
            "week" => Ok(GroupKey::Week),
            "day" => Ok(GroupKey::Day),
            "note" => Ok(GroupKey::Note),
            "message" => Ok(GroupKey::Message),
            _ => Err(FilterError::ArgumentError(s.to_string())),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Default)]
pub struct Group {
    pub key: String,
    pub subgroups: HashMap<String, Group>,
    pub items: Vec<Entry>,
    pub level: usize,
}

impl Group {
    fn create_subgroup(&mut self, key: &str) {
        self.subgroups.insert(
            key.to_string(),
            Group {
                key: key.to_string(),
                level: self.level + 1,
                ..Default::default()
            },
        );
    }

    fn push_entry_down(&mut self, entry: Entry, keys: Vec<String>) {
        if keys.is_empty() {
            self.items.push(entry);
            return;
        }

        let (key, rest) = keys.split_first().unwrap();

        if !self.subgroups.contains_key(key) {
            self.create_subgroup(key);
        }

        self.subgroups
            .get_mut(key)
            .unwrap()
            .push_entry_down(entry, rest.to_vec());
    }

    pub fn sumarize_len(&self, recursive: bool) -> usize {
        if recursive {
            self.items.len()
                + self
                    .subgroups
                    .values()
                    .fold(0, |p, v| p + v.sumarize_len(true))
        } else {
            self.items.len()
        }
    }

    pub fn sumarize_cnt(&self, recursive: bool) -> i32 {
        let count = self.items.iter().fold(0, |p, e| p + e.count());
        if recursive {
            self.subgroups
                .values()
                .fold(count, |p, v| p + v.sumarize_cnt(true))
        } else {
            count
        }
    }

    pub fn sumarize_duration(&self, recursive: bool) -> Duration {
        let duration = self.items.iter().fold(Duration::zero(), |p, i| {
            p + i.duration.unwrap_or_else(Duration::zero)
        });

        if recursive {
            self.subgroups
                .values()
                .fold(duration, |p, v| p + v.sumarize_duration(true))
        } else {
            duration
        }
    }
}

impl fmt::Display for Group {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let margin = "    ".to_string().repeat(self.level as usize);
        writeln!(
            f,
            "{}[{}]: cnt: {} duration: {}",
            margin,
            self.key,
            self.sumarize_cnt(true),
            self.sumarize_duration(true)
        )?;
        for i in self.items.iter() {
            writeln!(f, "{} - {:?}", margin, i)?;
        }

        let mut groups: Vec<String> =
            self.subgroups.keys().map(|g| g.to_owned()).collect();
        groups.sort();
        groups
            .iter()
            .map(|g| self.subgroups.get(g).unwrap())
            .for_each(|g| {
                writeln!(f, "{}", g).unwrap();
            });
        Ok(())
    }
}

#[derive(Debug, Eq, PartialEq, Default)]
pub struct Grouper {
    // grouping vector
    keys: Vec<GroupKey>,
    max_depth: usize,
    flat: bool,
    pub group: Group,
}

impl Grouper {
    pub fn new(max_depth: usize, flat: bool) -> Grouper {
        Grouper {
            max_depth,
            flat,
            ..Default::default()
        }
    }

    pub fn add_level(&mut self, key: &str) -> Result<bool, FilterError> {
        let key = GroupKey::from_str(key)?;
        if self.max_depth > 0 && self.keys.len() >= self.max_depth {
            return Ok(false);
        }

        self.keys.push(key);
        Ok(true)
    }

    pub fn depth(&self) -> usize {
        self.keys.len()
    }

    pub fn items(&self) -> &Group {
        &self.group
    }
}

impl Register for Grouper {
    fn append_entry(&mut self, entry: Entry) {
        let keys = self
            .keys
            .iter()
            .map(|k| match k {
                GroupKey::Year => vec![entry.year().unwrap_or_default()],
                GroupKey::Month => vec![entry.month().unwrap_or_default()],
                GroupKey::Quarter => {
                    vec![entry.quarter().unwrap_or_default()]
                }
                GroupKey::Week => vec![entry.week().unwrap_or_default()],
                GroupKey::Day => vec![entry.day().unwrap_or_default()],
                GroupKey::Project(levels) => entry.project_as_vec(*levels),
                GroupKey::Note => vec![entry.note.clone()],
                GroupKey::Message => vec![entry.message()],
            })
            .flatten()
            .collect::<Vec<String>>();

        let keys = if self.flat {
            vec![keys.join(": ")]
        } else {
            keys
        };
        self.group.push_entry_down(entry, keys);
    }
}
