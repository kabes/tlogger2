//
// raports.rs
// Copyright (C) 2020-2021, Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use std::io::Write;

use super::common_args;
use super::errors::CliError;
use super::CliCtx;
use crate::register::{
    DefaultRegister, Entry, EntryType, Filter, Group, Grouper,
};

pub fn report(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    let mut filter = common_args::prepare_filter(args)?;
    if !args.is_present("type") {
        filter.add_type(vec!["task"]);
        filter.prepare()?
    }

    // show filters
    if !filter.is_empty() && ctx.verbose() {
        println!("Filters: {}\n", filter.info());
    }

    let max_depth = if let Some(max_depth) = args.value_of("max_depth") {
        match max_depth.parse::<usize>() {
            Ok(0) | Err(_) => {
                return Err(CliError::ArgumentError(
                    "invalid max-depth".into(),
                ));
            }
            Ok(d) => d,
        }
    } else {
        9
    };

    let flat = args.is_present("flat");

    let mut group = Grouper::new(max_depth, flat);

    if let Some(groups) = args.values_of("group") {
        groups
            .map(|arg| {
                group.add_level(arg).map_err(|_| {
                    CliError::ArgumentError(format!(
                        "invalid group name: {}",
                        arg
                    ))
                })
            })
            .collect::<Result<Vec<_>, _>>()?;
    } else {
        group.add_level("message").unwrap();
    }

    let filenames = ctx.input_file_names()?;
    crate::io::loader::load_file(filenames, Some(filter), &mut group)?;

    let out_writer = common_args::prepare_write(args)?;

    let mut formatter: Box<dyn ReportFormatter> = match args
        .value_of("output_format")
    {
        Some("csv") => Box::new(CsvFormatter::new(out_writer, group.depth())),
        _ => Box::new(PlainFormatter::new(out_writer)),
    };

    if group.depth() > 0 {
        formatter
            .write_header_g()
            .and_then(|_| display_group(&mut formatter, group.items(), &[]))?;
    } else {
        display_nogrouped(&mut formatter, group.items())?;
    };

    Ok(())
}

fn display_group(
    writer: &mut Box<dyn ReportFormatter>,
    g: &Group,
    path: &[String],
) -> Result<(), CliError> {
    //    debug!("group {:?}", g);

    if g.level > 0 {
        writer.write_group(g, path)?;
    }

    let mut path = path.to_owned();
    path.push(g.key.clone());

    let mut groups: Vec<String> =
        g.subgroups.keys().map(|g| g.to_owned()).collect();
    groups.sort();
    groups
        .iter()
        .map(|sgn| g.subgroups.get(sgn).unwrap())
        .map(|sg| display_group(writer, sg, &path))
        .collect::<Result<Vec<_>, CliError>>()?;

    if g.level == 0 {
        writer.write_separator()?;
        writer.write_summary(g)?;
    }

    Ok(())
}

fn display_nogrouped(
    writer: &mut Box<dyn ReportFormatter>,
    g: &Group,
) -> Result<(), CliError> {
    writer.write_header_ng()?;

    g.items
        .iter()
        .map(|i| {
            trace!("entry: {:?}", i);
            writer.write_entry(i)
        })
        .collect::<Result<Vec<_>, CliError>>()?;

    writer.write_separator()?;
    writer.write_summary_ng(g)?;
    Ok(())
}

type FormatterResult = Result<(), super::errors::CliError>;

trait ReportFormatter {
    fn write_header_ng(&mut self) -> FormatterResult;
    fn write_header_g(&mut self) -> FormatterResult;
    fn write_entry(&mut self, e: &Entry) -> FormatterResult;
    fn write_group(&mut self, g: &Group, path: &[String]) -> FormatterResult;
    fn write_separator(&mut self) -> FormatterResult;
    fn write_summary(&mut self, g: &Group) -> FormatterResult;
    fn write_summary_ng(&mut self, g: &Group) -> FormatterResult;
    fn flush(&mut self) -> FormatterResult;
}

struct PlainFormatter<W: Write> {
    writer: W,
}

impl<W: Write> PlainFormatter<W> {
    fn new(writer: W) -> Self {
        PlainFormatter { writer }
    }
}

static SEPARATOR : &str = "==================================================================================================";

impl<W: Write> ReportFormatter for PlainFormatter<W> {
    fn write_header_ng(&mut self) -> FormatterResult {
        writeln!(
            self.writer,
            "{:<50}   {:>12}   {:>5}",
            "Title", "Duration", "Count"
        )?;
        writeln!(self.writer, "{}", SEPARATOR)?;
        Ok(())
    }

    fn write_header_g(&mut self) -> FormatterResult {
        writeln!(
            self.writer,
            "{:<60}   {:>12}   {:>5}   {:>5}",
            "Title", "Duration", "Count", "Entr."
        )?;
        writeln!(self.writer, "{}", SEPARATOR)?;
        Ok(())
    }

    fn write_entry(&mut self, e: &Entry) -> FormatterResult {
        if e.entry_type != EntryType::Task {
            return Ok(());
        }
        writeln!(
            self.writer,
            "{:<50}   {:>12}   {:>5}",
            e.content,
            e.duration.map(|d| d.to_string()).unwrap_or_default(),
            e.count().to_string(),
        )?;
        Ok(())
    }

    fn write_group(&mut self, g: &Group, _path: &[String]) -> FormatterResult {
        let prefix = "  ".to_string().repeat(g.level - 1);
        let duration = g.sumarize_duration(true).to_string();
        let cnt = match g.sumarize_cnt(true) {
            0 => "".to_string(),
            v => format!("{}", v),
        };
        writeln!(
            self.writer,
            "{:<60}   {:>12}   {:>5}   {:>5}",
            prefix + &g.key,
            duration,
            cnt,
            g.sumarize_len(true)
        )?;
        Ok(())
    }

    fn write_separator(&mut self) -> FormatterResult {
        writeln!(self.writer, "{}", SEPARATOR)?;
        Ok(())
    }

    fn write_summary(&mut self, g: &Group) -> FormatterResult {
        let prefix = if g.level > 1 {
            "  ".to_string().repeat(g.level - 1)
        } else {
            "".to_owned()
        };
        writeln!(
            self.writer,
            "{:<60}   {:>12}   {:>5}   {:>5}",
            prefix + "Total",
            g.sumarize_duration(true).to_string(),
            g.sumarize_cnt(true).to_string(),
            g.sumarize_len(true).to_string()
        )?;
        Ok(())
    }

    fn write_summary_ng(&mut self, g: &Group) -> FormatterResult {
        writeln!(
            self.writer,
            "{:<50}   {:>12}   {:>5}",
            "Total",
            g.sumarize_duration(true).to_string(),
            g.sumarize_cnt(true).to_string()
        )?;
        Ok(())
    }

    fn flush(&mut self) -> FormatterResult {
        Ok(())
    }
}

struct CsvFormatter<W: Write> {
    writer: csv::Writer<W>,
    depth: usize,
}

impl<W: Write> CsvFormatter<W> {
    fn new(writer: W, depth: usize) -> Self {
        let writer = csv::Writer::from_writer(writer);
        CsvFormatter { writer, depth }
    }
}

impl<W: Write> ReportFormatter for CsvFormatter<W> {
    fn write_header_ng(&mut self) -> FormatterResult {
        self.writer.write_record(&[
            "Data", "Project", "Note", "Comment", "Duration", "Count",
        ])?;
        Ok(())
    }

    fn write_header_g(&mut self) -> FormatterResult {
        let mut header = vec!["Title"];
        header.resize(self.depth, "");
        header.append(&mut vec!["Duration", "Count", "Entries"]);
        self.writer.write_record(&header)?;
        Ok(())
    }

    fn write_entry(&mut self, e: &Entry) -> FormatterResult {
        if e.entry_type != EntryType::Task {
            return Ok(());
        }
        self.writer.write_record(&vec![
            &e.date_time.map(|d| d.to_string()).unwrap_or_default(),
            &e.project
                .as_ref()
                .map_or_else(|| String::from(""), |v| v.clone()),
            &e.note,
            &e.comment
                .as_ref()
                .map_or_else(|| String::from(""), |d| d.to_string()),
            &e.duration.map(|d| d.to_string()).unwrap_or_default(),
            &e.count().to_string(),
        ])?;
        Ok(())
    }

    fn write_group(&mut self, g: &Group, path: &[String]) -> FormatterResult {
        let mut record = vec![];
        path[1..].iter().for_each(|p| record.push(p.to_string()));
        record.push(g.key.clone());
        record.append(&mut vec!["".to_string(); self.depth - g.level]);
        record.push({
            let d = g.sumarize_duration(true);
            if d.is_zero() {
                "".to_string()
            } else {
                d.to_string()
            }
        });
        record.push(match g.sumarize_cnt(true) {
            0 => "".to_string(),
            v => format!("{}", v),
        });
        record.push(g.sumarize_len(true).to_string());
        self.writer.write_record(record)?;
        Ok(())
    }

    fn write_separator(&mut self) -> FormatterResult {
        Ok(())
    }

    fn write_summary(&mut self, g: &Group) -> FormatterResult {
        let mut rec = vec![];
        if g.level > 1 {
            rec.append(&mut vec![""; g.level - 1]);
        }
        rec.push("Total");
        rec.append(&mut vec![""; self.depth - g.level - 1]);
        let dur = g.sumarize_duration(true).to_string();
        let cnt = g.sumarize_cnt(true).to_string();
        let len = g.sumarize_len(true).to_string();
        rec.append(&mut vec![&dur, &cnt, &len]);
        self.writer.write_record(rec)?;
        Ok(())
    }

    fn write_summary_ng(&mut self, g: &Group) -> FormatterResult {
        self.writer.write_record(vec![
            "Total",
            "",
            "",
            "",
            &g.sumarize_duration(true).to_string(),
            &g.sumarize_cnt(true).to_string(),
        ])?;
        Ok(())
    }

    fn flush(&mut self) -> FormatterResult {
        self.writer.flush()?;
        Ok(())
    }
}

//----------------------------

fn prepare_filter_for_status() -> Filter {
    let mut filter = Filter::new();
    filter.add_range("today");
    filter.prepare().unwrap();
    filter
}

pub fn status(ctx: &CliCtx, _args: &clap::ArgMatches) -> Result<(), CliError> {
    let filter = prepare_filter_for_status();
    // show filters
    if !filter.is_empty() && ctx.verbose() {
        println!("Filters: {}\n", filter.info());
    }

    let filenames = ctx.input_file_names()?;
    let mut r = DefaultRegister::default();
    crate::io::loader::load_file(filenames, Some(filter), &mut r)?;

    if r.entries.is_empty() {
        println!("no entries today");
        return Ok(());
    }

    println!("Today entries:");
    for e in r.entries.iter() {
        let dt = e
            .date_time
            .map_or_else(|| "".to_string(), |d| d.time().to_string());
        let project = e
            .project
            .as_ref()
            .map_or_else(|| "".to_string(), |pr| pr.to_string() + " ");

        match e.entry_type {
            EntryType::Marker | EntryType::Note => {
                println!("  {:>8}                          {}", dt, e.note)
            }
            EntryType::Task => println!(
                "  {:>8} - {:>8}  {:>10}   {}{}",
                e.start_date_time.unwrap().time(),
                dt,
                e.duration.unwrap().to_string(),
                project,
                e.note
            ),
            _ => (),
        }
    }

    if let Some(fl) = r.duration_from_last() {
        println!();
        println!("From last: {}", fl);
    }

    Ok(())
}
