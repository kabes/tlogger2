//
// add.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use super::{CliCtx, CliError};
use crate::io;
use std::convert::TryFrom;

use crate::register::{
    DefaultRegister, Duration, Filter, LastMessagesRegister,
};

fn now() -> String {
    let date = chrono::Local::now();
    let date = date.naive_local();
    date.format("%Y-%m-%d %H:%M").to_string()
}

/// Add new entry do file. If no INPUT - show last entries and allow select one.
pub fn add(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    let mut input = args
        .value_of("INPUT")
        .unwrap_or_default()
        .trim()
        .to_string();

    if input.is_empty() {
        match ctx.input_file_names().and_then(get_last_entries) {
            Err(e) => {
                return Err(CliError::IoErrorStr(format!(
                    "load last entries error: {}",
                    e
                )))
            }
            Ok(None) => {
                return Ok(());
            }
            Ok(i) => {
                input = i.unwrap();
            }
        };
    }

    let content = format!("{}: {}", now(), input);
    let filename = ctx.writeable_file_name()?;
    if let Some(fl) = add_entries(filename, vec![content])? {
        println!("   from last: {}", fl.human());
    }

    Ok(())
}

/// Add note to file.
pub fn add_note(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    let mut input = args
        .value_of("INPUT")
        .ok_or(CliError::MissingArgument("INPUT"))?
        .trim()
        .to_string();

    if input.is_empty() {
        return Err(CliError::MissingArgument("INPUT"));
    }

    if !input.starts_with('#') {
        input = "# ".to_owned() + input.trim();
    }

    let content = format!("{}: {}", now(), input);
    let filename = ctx.writeable_file_name()?;
    if let Some(fl) = add_entries(filename, vec![content])? {
        println!("   from last: {}", fl.human());
    }
    Ok(())
}

pub fn add_start(
    ctx: &CliCtx,
    args: &clap::ArgMatches,
) -> Result<(), CliError> {
    let input = args.value_of("INPUT").map_or_else(
        || "start **".to_owned(),
        |i| {
            let i = i.trim();
            if i.ends_with("**") {
                i.to_owned()
            } else {
                i.to_owned() + " **"
            }
        },
    );

    let content = format!("{}: {}", now(), input);
    let filename = ctx.writeable_file_name()?;
    if let Some(fl) = add_entries(filename, vec![content])? {
        println!("   from last: {}", fl.human());
    }
    Ok(())
}

pub fn add_end(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    let filename = ctx.writeable_file_name()?;

    let input = get_input_or_ask(args, ctx.input_file_names()?)?;

    // add start marker
    let duration = args.value_of("duration").ok_or_else(|| {
        CliError::ArgumentError("duration is required".into())
    })?;
    let duration = Duration::try_from(duration)
        .map_err(|_| CliError::ArgumentError("invalid duration".into()))?;
    trace!("duration: {:?}", duration);

    let start_marker = {
        let date = chrono::Local::now().naive_local()
            - chrono::Duration::seconds(duration.into());
        date.format("%Y-%m-%d %H:%M: start **").to_string()
    };
    let content = format!("{}: {}", now(), input);

    let entries = vec![start_marker, content];
    add_entries(filename, entries)?;
    Ok(())
}

fn prepare_filter_for_add_last() -> Filter {
    let mut filter = Filter::new();
    filter.add_range("past-month");
    filter.add_type(vec!["t"]);
    filter.prepare().unwrap();
    filter
}

/// get input parameter value or ask/select for message
fn get_input_or_ask(
    args: &clap::ArgMatches,
    filenames: Vec<&str>,
) -> Result<String, CliError> {
    if let Some(i) = args.value_of("INPUT").map(|s| s.trim()) {
        if !i.is_empty() {
            return Ok(String::from(i));
        }
    }

    if let Some(f) = get_last_entries(filenames)? {
        Ok(f)
    } else {
        Err(CliError::ArgumentError("missing input".into()))
    }
}

fn get_last_entries(filenames: Vec<&str>) -> Result<Option<String>, CliError> {
    let filter = prepare_filter_for_add_last();
    let mut r = LastMessagesRegister::new(10);
    io::loader::load_file(filenames, Some(filter), &mut r)?;

    let last_msg = r.messages;
    if last_msg.is_empty() {
        println!("No previous messages...");
        return Ok(None);
    }

    println!("Last messages:");
    last_msg
        .iter()
        .enumerate()
        .for_each(|(i, m)| println!(" {:>2}. {}", i + 1, m));

    loop {
        print!("Enter number or new message: ");
        use std::io::Write;
        std::io::stdout().flush().unwrap();
        let line = {
            let mut line = String::new();
            std::io::stdin().read_line(&mut line).unwrap();
            line.trim().to_string()
        };

        if line.is_empty() {
            return Ok(None);
        }

        if let Ok(num) = line.parse::<usize>() {
            if num > last_msg.len() {
                eprintln!("wrong number: out of range");
            } else {
                return Ok(Some(last_msg[num - 1].to_string()));
            }
        } else {
            return Ok(Some(line));
        }
    }
}

/// Write entries to log file.
/// Add separator when previous entry is other day than today.
pub fn add_entries(
    filename: &str,
    lines: Vec<String>,
) -> Result<Option<Duration>, CliError> {
    let mut r = DefaultRegister::default();
    io::loader::quick_load_last_entry(vec![filename], &mut r)?;

    let mut content = lines.join("\n");

    if let Some(last_entry) = r.entries.last() {
        let date = chrono::Local::now();
        let date = date.naive_local();

        if let Some(last_entry_day) = last_entry.day() {
            if last_entry_day != date.format("%Y-%m-%d").to_string() {
                content = "\n".to_string() + &content;
            }
        }
    }

    io::writer::append(filename, &content)?;

    println!("added: {}", content);
    Ok(r.duration_from_last())
}
