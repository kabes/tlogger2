//
// errors.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use thiserror::Error;

use crate::conv::parse_date;
use crate::register::errors::FilterError;

#[derive(Error, Debug)]
pub enum CliError {
    #[error("{0}")]
    ArgumentError(String),

    #[error("missing argument: {0}")]
    MissingArgument(&'static str),

    #[error(transparent)]
    ParseDateError(#[from] parse_date::ArgumentError),

    #[error("{0}")]
    FilterPrepareError(#[from] FilterError),

    #[error("{0}")]
    IoError(#[from] std::io::Error),

    #[error(transparent)]
    AppIoError(#[from] crate::io::errors::IoError),

    #[error("{0}")]
    IoErrorStr(String),

    #[error("{0}")]
    CsvError(#[from] csv::Error),
}
