//
// tools.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use std::convert::TryFrom;
use std::io::Write;
use std::process::Command;
use std::{env, thread, time};

use crate::register::Duration;

use super::add;
use super::{CliCtx, CliError};

pub fn pomodoro(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    // add start marker
    let duration = args
        .value_of("duration")
        .unwrap_or_else(|| ctx.conf.pomodoro_default_duration.as_ref());
    let duration = Duration::try_from(duration).map_err(|_| {
        CliError::ArgumentError(format!("invalid duration: {}", duration))
    })?;
    trace!("duration: {:?}", duration);

    add::add_start(ctx, args)?;

    let finish_command = get_finish_command(ctx, args);

    loop {
        println!("Pomodoro {} - START!", &duration);
        let res = pomodoro_loop(&duration, finish_command);
        if res {
            print!("Continue? [y/N]: ");
            std::io::stdout().flush().unwrap();
            let line = {
                let mut line = String::new();
                std::io::stdin().read_line(&mut line).unwrap();
                line.trim().to_string()
            };
            if line.to_lowercase().starts_with('y') {
                continue;
            }

            add::add(ctx, args)?;
            break;
        }
    }
    Ok(())
}

fn pomodoro_loop(duration: &Duration, finish_command: Option<&str>) -> bool {
    let duration: chrono::Duration = duration.into();
    let end_ts = chrono::Local::now() + duration;
    println!();
    loop {
        let now = chrono::Local::now();
        let left = end_ts - now;
        let left: Duration = left.into();
        print!("\rLeft: {}              ", left);
        std::io::stdout().flush().unwrap();
        if left.is_zero() {
            if let Some(cmd) = finish_command {
                launch_pomodoro_cmd(cmd);
            }

            print!("\r              \r");
            return true;
        }
        let ten_millis = time::Duration::from_secs(1);
        thread::sleep(ten_millis);
    }
}

pub fn editor(ctx: &CliCtx, _args: &clap::ArgMatches) -> Result<(), CliError> {
    let filename = ctx.writeable_file_name()?;
    let editor = match env::var_os("EDITOR") {
        Some(e) => e,
        None => {
            return Err(CliError::ArgumentError(
                "missing EDITOR environment setting".into(),
            ));
        }
    };
    debug!("launching {:?} {:?}", &editor, &filename);
    Command::new(editor)
        .args(&[filename])
        .spawn()
        .map(|_| ())
        .map_err(CliError::IoError)
}

fn launch_pomodoro_cmd(cmd: &str) {
    if cmd.is_empty() {
        return;
    }

    use shlex::split;

    if let Some(cmd) = split(cmd) {
        debug!("cmd: {:?}", cmd);

        let mut exec = Command::new(&cmd[0]).args(&cmd[1..]).spawn();
        if let Ok(child) = &mut exec {
            child.wait().unwrap();
        } else {
            warn!("command error: {:?}", exec);
        }
    }
}

pub fn last_entry(
    ctx: &CliCtx,
    _args: &clap::ArgMatches,
) -> Result<(), CliError> {
    let filename = ctx.writeable_file_name()?;
    let mut r = crate::register::DefaultRegister::default();
    crate::io::loader::quick_load_last_entry(vec![filename], &mut r)?;

    println!("last: {:?}", r.entries.last());
    let dur = r.duration_from_last().unwrap();
    println!("duration: {:?}", dur);
    println!("duration: {}", dur);

    Ok(())
}

fn get_finish_command<'a>(
    ctx: &'a CliCtx,
    args: &'a clap::ArgMatches,
) -> Option<&'a str> {
    if let Some(cmd) = args.value_of("command") {
        if !cmd.is_empty() {
            return Some(cmd);
        }
    }

    if ctx.conf.pomodoro_finish_cmd.is_empty() {
        None
    } else {
        Some(&ctx.conf.pomodoro_finish_cmd)
    }
}
