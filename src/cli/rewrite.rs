//
// rewrite.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use super::{CliCtx, CliError};
use crate::io;

pub fn undo(ctx: &CliCtx, _args: &clap::ArgMatches) -> Result<(), CliError> {
    let filename = ctx.writeable_file_name()?;
    let res = io::rewrite::remove_last_entry(filename)?;
    println!("removed: {}", res);
    Ok(())
}

pub fn archive(ctx: &CliCtx, _args: &clap::ArgMatches) -> Result<(), CliError> {
    let filename = ctx.writeable_file_name()?;
    let res = io::archive::archive(filename)?;
    println!("moved {} entries", res);
    Ok(())
}
