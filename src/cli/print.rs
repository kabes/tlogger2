//
// print.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use std::io::Write;

use super::common_args;
use super::{CliCtx, CliError};
use crate::register::DefaultRegister;
use crate::register::Entry;

pub fn print(ctx: &CliCtx, args: &clap::ArgMatches) -> Result<(), CliError> {
    let filter = common_args::prepare_filter(args)?;

    let verbose = ctx.verbose();
    // show filters
    if !filter.is_empty() && verbose {
        println!("Filters: {}\n", filter.info());
    }

    let filenames = ctx.input_file_names()?;
    let mut r = DefaultRegister::default();
    crate::io::loader::load_file(filenames, Some(filter), &mut r)?;
    let out_writer = common_args::prepare_write(args)?;

    let mut formatter: Box<dyn OutputWriter> =
        match args.value_of("output_format") {
            Some("csv") => Box::new(CsvOW::new(out_writer, verbose)),
            _ => Box::new(SimpleOW::new(out_writer, verbose)),
        };

    formatter.write_header()?;
    for e in r.entries {
        formatter.write_entry(&e)?;
    }
    formatter.flush()?;
    Ok(())
}

type OuputWritterResult = Result<(), super::errors::CliError>;

trait OutputWriter {
    fn flush(&mut self) -> OuputWritterResult;
    fn write_header(&mut self) -> OuputWritterResult;
    fn write_entry(&mut self, e: &Entry) -> OuputWritterResult;
}

struct SimpleOW<W: Write> {
    writer: W,
    verbose: bool,
}

impl<W: Write> SimpleOW<W> {
    fn new(writer: W, verbose: bool) -> Self {
        SimpleOW { writer, verbose }
    }
}

impl<W: Write> OutputWriter for SimpleOW<W> {
    fn write_header(&mut self) -> OuputWritterResult {
        Ok(())
    }

    fn write_entry(&mut self, e: &Entry) -> OuputWritterResult {
        writeln!(self.writer, "{}", e.print(self.verbose))
            .map(|_| ())
            .map_err(|e| CliError::IoErrorStr(e.to_string()))
    }

    fn flush(&mut self) -> OuputWritterResult {
        Ok(())
    }
}

struct CsvOW<W: Write> {
    writer: csv::Writer<W>,
    verbose: bool,
}

impl<W: Write> CsvOW<W> {
    fn new(writer: W, verbose: bool) -> Self {
        let writer = csv::Writer::from_writer(writer);
        CsvOW { writer, verbose }
    }
}

impl<W: Write> OutputWriter for CsvOW<W> {
    fn write_header(&mut self) -> OuputWritterResult {
        if self.verbose {
            self.writer.write_record(&[
                "Data", "Project", "Note", "Comment", "Duration",
            ])?;
        } else {
            self.writer
                .write_record(&["Data", "Project", "Note", "Comment"])?;
        }

        Ok(())
    }

    fn write_entry(&mut self, e: &Entry) -> OuputWritterResult {
        let mut rec = vec![
            e.date_time.map(|d| d.to_string()).unwrap_or_default(),
            e.project
                .as_ref()
                .map_or_else(|| String::from(""), |v| v.clone()),
            e.note.clone(),
            e.comment
                .as_ref()
                .map_or_else(|| String::from(""), |d| d.to_string()),
        ];
        if self.verbose {
            rec.push(e.duration.map(|d| d.to_string()).unwrap_or_default());
        }

        self.writer.write_record(&rec)?;
        Ok(())
    }

    fn flush(&mut self) -> OuputWritterResult {
        self.writer.flush()?;
        Ok(())
    }
}

pub fn print_projects(
    ctx: &CliCtx,
    args: &clap::ArgMatches,
) -> Result<(), CliError> {
    let filter = common_args::prepare_filter(args)?;

    // show filters
    if !filter.is_empty() && ctx.verbose() {
        println!("Filters: {}\n", filter.info());
    }

    let filenames = ctx.input_file_names()?;
    let mut r = DefaultRegister::default();
    crate::io::loader::load_file(filenames, Some(filter), &mut r)?;
    let mut out_writer = common_args::prepare_write(args)?;

    for e in r.projects() {
        writeln!(out_writer, "{}", e.strip_suffix(':').unwrap()).unwrap();
    }

    Ok(())
}
