//
// common_args.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use std::fs::File;
use std::io::BufWriter;
use std::io::{self, Write};

use super::errors::CliError;
use crate::conv::parse_date::parse_date_str;
use crate::register::Filter;

pub fn prepare_filter(args: &clap::ArgMatches) -> Result<Filter, CliError> {
    let mut filter = Filter::new();

    if let Some(input) = args.value_of("period") {
        filter.add_range(input);
    }

    if let Some(start) = args.value_of("begin") {
        let start = parse_date_str(start, false)?;
        filter.add_min_date(start);
        trace!("add filter: begin {:?}", &start);
    };
    if let Some(end) = args.value_of("end") {
        let end = parse_date_str(end, true)?;
        filter.add_max_date(end);
        trace!("add filter: end {:?}", &end);
    };
    if let Some(projects) = args.values_of("project").map(|p| p.collect()) {
        trace!("add filter projects: {:?}", &projects);
        filter.add_projects(projects);
    };
    if let Some(text) = args.value_of("contains") {
        filter.add_str(text);
    };
    let entry_types = args
        .values_of("type")
        .map(|et| et.collect::<Vec<&str>>())
        .unwrap_or_default();
    if !entry_types.is_empty() {
        filter.add_type(entry_types);
    }

    if let Some(inputs) = args.values_of("INPUT") {
        let input = inputs
            .map(|i| i.to_string())
            .collect::<Vec<String>>()
            .join(" ");
        filter.push(&input);
    }

    filter.prepare()?;

    Ok(filter)
}

/// Prepare output stream (file or stdout)
pub fn prepare_write(
    args: &clap::ArgMatches,
) -> Result<BufWriter<Box<dyn Write>>, CliError> {
    let out_writer: Box<dyn Write> = {
        let ofilename = args.value_of("output").unwrap_or("-");
        if ofilename == "-" {
            Box::new(io::stdout())
        } else {
            let f = File::create(ofilename)?;
            Box::new(f)
        }
    };

    Ok(BufWriter::new(out_writer))
}

/// Get all filenames from global parameters
pub fn get_filenames<'a>(
    global: &'a clap::ArgMatches,
    conf: &'a crate::conf::Config,
) -> Result<Vec<&'a str>, CliError> {
    let filenames = global
        .values_of("file")
        .unwrap()
        .filter(|e| !e.is_empty())
        .collect::<Vec<&str>>();

    debug!("files to load {:?}", filenames);
    if !filenames.is_empty() {
        return Ok(filenames);
    }

    if !conf.tlogger_file.is_empty() {
        return Ok(vec![&conf.tlogger_file]);
    }

    Err(CliError::ArgumentError("missing log file name".into()))
}

/// Get last filename from global parameters; is used to write into
pub fn get_filename_for_write<'a>(
    global: &'a clap::ArgMatches,
    conf: &'a crate::conf::Config,
) -> Result<&'a str, CliError> {
    let filename = global
        .values_of("file")
        .unwrap()
        .filter(|f| !f.is_empty())
        .last()
        .or_else(|| {
            if conf.tlogger_file.is_empty() {
                None
            } else {
                Some(conf.tlogger_file.as_ref())
            }
        })
        .ok_or_else(|| CliError::ArgumentError("missing log file name".into()));

    debug!("last file: {:?}", filename);
    filename
}
