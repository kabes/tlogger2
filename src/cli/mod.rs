//
// mod.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

mod add;
mod common_args;
mod errors;
mod print;
mod reports;
mod rewrite;
mod tools;

pub use add::{add, add_end, add_note, add_start};
pub use errors::CliError;
pub use print::{print, print_projects};
pub use reports::{report, status};
pub use rewrite::{archive, undo};
pub use tools::{editor, last_entry, pomodoro};

use crate::conf;

pub struct CliCtx<'a> {
    pub global: &'a clap::ArgMatches,
    pub conf: &'a conf::Config,
}

impl<'a> CliCtx<'a> {
    pub fn new(global: &'a clap::ArgMatches, conf: &'a conf::Config) -> Self {
        CliCtx { global, conf }
    }

    pub fn writeable_file_name(&self) -> Result<&'a str, CliError> {
        common_args::get_filename_for_write(self.global, self.conf)
    }

    pub fn input_file_names(&self) -> Result<Vec<&'a str>, CliError> {
        common_args::get_filenames(self.global, self.conf)
    }

    pub fn verbose(&self) -> bool {
        self.global.occurrences_of("verbose") > 0
    }
}
