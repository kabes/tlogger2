//
// loader.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use super::errors::{IoError, ParseLineError};
use crate::register::Filter;
use crate::register::{Element, Parser, Register};
use crate::utils::iterutils::until_err;

pub fn load_file(
    filenames: Vec<&str>,
    filter: Option<Filter>,
    register: &mut dyn Register,
) -> Result<(), IoError> {
    let mut filter = filter;

    for filename in filenames {
        debug!("loading {}", filename);
        let contents = std::fs::read_to_string(filename)
            .map_err(|e| IoError::new_load_error(filename.to_string(), e))?;

        load_str(contents.lines(), register, &mut filter).map_err(|e| {
            IoError::ParseFileError {
                filename: filename.to_string(),
                source: e,
            }
        })?;
    }

    Ok(())
}

fn load_last_line(
    filename: &str,
) -> Result<Option<(String, String, usize)>, IoError> {
    debug!("loading {}", filename);
    let res = std::fs::read_to_string(filename)
        .map_err(|e| IoError::new_load_error(filename.to_string(), e))?
        .lines()
        .map(|line| line.trim())
        .enumerate()
        .filter(|(_line_no, line)| !(line.is_empty() || line.starts_with('#')))
        .last()
        .map(|(line_no, line)| {
            (filename.to_string(), line.to_string(), line_no)
        });
    Ok(res)
}

/// Quick load only last line that looks like entry
pub fn quick_load_last_entry(
    filenames: Vec<&str>,
    register: &mut dyn Register,
) -> Result<(), IoError> {
    let mut err = Ok(());
    let last_line = filenames
        .iter()
        .map(|filename| load_last_line(filename))
        .scan(&mut err, until_err)
        .last();

    if let Some(Some((filename, line, line_no))) = last_line {
        let mut parser = Parser::new();
        let res = parser.parse_line(&line).map_err(|err| {
            IoError::ParseFileError {
                filename,
                source: ParseLineError { line_no, err },
            }
        })?;
        if let Element::Entry(entry) = res {
            register.append_entry(entry);
        }
    }

    err
}

fn load_str<'a, I>(
    s: I,
    r: &mut dyn Register,
    filter: &mut Option<Filter>,
) -> Result<(), ParseLineError>
where
    I: IntoIterator<Item = &'a str>,
{
    let mut parser = Parser::new();
    let mut err = Ok(());
    let items = s
        .into_iter()
        .enumerate()
        .map(|(line_no, line)| {
            parser
                .parse_line(line.trim_end())
                .map_err(|err| ParseLineError { line_no, err })
        })
        .scan(&mut err, until_err)
        .filter(|entry| match entry {
            Element::Entry(_) => true,
            e => {
                warn!("not supporrted: {}", e);
                false
            }
        });

    if let Some(f) = filter {
        items.for_each(|res| {
            if let Element::Entry(mut entry) = res {
                if f.is_match(&mut entry) {
                    r.append_entry(entry)
                }
            }
        });
    } else {
        items.for_each(|res| {
            if let Element::Entry(entry) = res {
                r.append_entry(entry);
            }
        });
    };

    err
}
