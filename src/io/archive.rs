//
// rewrite.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use regex::Regex;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;

use super::errors::IoError;

lazy_static! {
    static ref TAG_FILENAME: Regex =
        Regex::new(r"^(?P<base>.+?[_-]?)(?:\d{2}(?:\d{2})?)?\.[tT][xX][tT]$")
            .unwrap();
    static ref YEAR: Regex = Regex::new(r"^(?P<year>\d{4})-").unwrap();
}

struct OutputFile {
    file: File,
    year: String,
    filename: String,
}

impl OutputFile {
    fn new(base_filename: &str, year: &str) -> Result<Self, IoError> {
        let filename = format!("{}{}.txt", base_filename, year);
        debug!("opening file {}", &filename);
        let file = std::fs::OpenOptions::new()
            .create_new(true)
            .append(true)
            .write(true)
            .open(&filename)
            .map_err(|e| IoError::LoadError {
                filename: filename.to_owned(),
                source: e,
            })?;

        Ok(OutputFile {
            file,
            year: year.to_owned(),
            filename,
        })
    }

    fn append(&mut self, line: &str) -> Result<(), IoError> {
        writeln!(&mut self.file, "{}", line).map_err(|e| {
            IoError::WriteError {
                filename: self.filename.to_owned(),
                source: e,
            }
        })?;

        Ok(())
    }

    fn close(&mut self) -> Result<(), IoError> {
        debug!("closing file {}", &self.filename);
        self.file.flush().map_err(|e| IoError::WriteError {
            filename: self.filename.to_owned(),
            source: e,
        })
    }
}

// Remove and return last entry from given filename
pub fn archive(filename: &str) -> Result<usize, IoError> {
    trace!("opening {} for read", filename);
    let in_file = File::open(filename).map_err(|e| IoError::LoadError {
        filename: filename.to_string(),
        source: e,
    })?;

    let base_name: String = TAG_FILENAME
        .captures(filename)
        .map(|c| c.name("base").unwrap().as_str())
        .map(|b| {
            if b.ends_with('_') {
                b.to_string()
            } else {
                b.to_string() + "_"
            }
        })
        .ok_or_else(|| {
            IoError::OtherError("can't guess filename".to_string())
        })?;

    trace!("basename: {}", base_name);

    let mut current_out_file: Option<OutputFile> = None;

    let reader = BufReader::new(in_file);

    let mut counter: usize = 0;

    for line in reader.lines() {
        let line = line.map_err(|e| IoError::LoadError {
            filename: filename.to_string(),
            source: e,
        })?;
        if line.is_empty() {
            if let Some(out) = current_out_file.as_mut() {
                out.append(&line)?;
                counter += 1;
            } else {
                debug!("skipping empty file - no current file");
            }
        } else {
            match YEAR
                .captures(&line)
                .map(|c| c.name("year").unwrap().as_str())
            {
                Some(year) => {
                    if let Some(outfile) = current_out_file.as_mut() {
                        if year != outfile.year {
                            outfile.close()?;
                            current_out_file = None;
                        } else {
                            outfile.append(&line)?;
                            counter += 1;
                        }
                    }
                    if current_out_file.is_none() {
                        let mut outfile = OutputFile::new(&base_name, year)?;
                        outfile.append(&line)?;
                        counter += 1;
                        current_out_file = Some(outfile);
                    }
                }
                None => {
                    if let Some(outfile) = current_out_file.as_mut() {
                        outfile.append(&line)?;
                        counter += 1;
                    } else {
                        return Err(IoError::OtherError(format!(
                            "can't move line: {}",
                            &line
                        )));
                    }
                }
            }
        }
    }

    if current_out_file.is_none() {
        return Ok(0);
    }

    current_out_file.unwrap().close()?;

    // rename current year file
    use crate::chrono::Datelike;
    let current_year = chrono::Local::now().naive_local().year();
    let current_year_file = format!("{}{}.txt", base_name, current_year);
    if std::path::Path::new(&current_year_file).exists() {
        // create backup of current file
        let bak_filename = filename.to_string() + ".bak";
        std::fs::rename(filename, &bak_filename).map_err(|e| {
            IoError::GenericError {
                message: format!(
                    "rename {} to {} error",
                    filename, bak_filename
                ),
                source: e,
            }
        })?;
        // rename current year file
        std::fs::rename(&current_year_file, filename).map_err(|e| {
            IoError::GenericError {
                message: format!(
                    "rename {} to {} error",
                    &current_year_file, filename
                ),
                source: e,
            }
        })?;
    }

    Ok(counter)
}
