//
// mod.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

pub mod archive;
pub mod errors;
pub mod loader;
pub mod rewrite;
pub mod writer;

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn it_works() {}
// }
