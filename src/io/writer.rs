//
// writer.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use super::errors::IoError;
use std::io::Write;

pub fn append(filename: &str, content: &str) -> Result<(), IoError> {
    let mut f = std::fs::OpenOptions::new()
        .append(true)
        .write(true)
        .open(filename)
        .map_err(|e| IoError::new_write_error(filename.to_string(), e))?;

    writeln!(&mut f, "{}", content)
        .map_err(|e| IoError::new_write_error(filename.to_string(), e))?;

    Ok(())
}
