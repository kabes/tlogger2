//
// errors.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use crate::register::errors::ParseError;
use thiserror::Error;

#[derive(Error, Debug)]
#[error("line {line_no}: {err}")]
pub struct ParseLineError {
    pub line_no: usize,
    pub err: ParseError,
}

#[derive(Error, Debug)]
#[allow(clippy::enum_variant_names)]
pub enum IoError {
    #[error(
        "parse file {filename} line {} error: {}", (.source.line_no + 1),  .source.err
    )]
    ParseFileError {
        source: ParseLineError,
        filename: String,
    },

    #[error("read file '{filename}' error: {source}")]
    LoadError {
        filename: String,
        source: std::io::Error,
    },

    #[error("write file '{filename}' error: {source}")]
    WriteError {
        filename: String,
        source: std::io::Error,
    },

    #[error("{0}: {source}")]
    GenericError {
        message: String,
        source: std::io::Error,
    },

    #[error("operation error: {0}")]
    OtherError(String),
}

impl IoError {
    pub fn new_load_error(filename: String, source: std::io::Error) -> IoError {
        IoError::LoadError { filename, source }
    }

    pub fn new_write_error(
        filename: String,
        source: std::io::Error,
    ) -> IoError {
        IoError::WriteError { filename, source }
    }
}
