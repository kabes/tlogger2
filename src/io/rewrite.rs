//
// rewrite.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;

use super::errors::IoError;

// Remove and return last entry from given filename
pub fn remove_last_entry(filename: &str) -> Result<String, IoError> {
    trace!("opening {} for read", filename);
    let in_file = File::open(filename).map_err(|e| IoError::LoadError {
        filename: filename.to_string(),
        source: e,
    })?;
    let reader = BufReader::new(in_file);

    let temp_filename = filename.to_string() + ".tmp";
    let temp_filename = &temp_filename;
    trace!("opening {} for write", temp_filename);
    let mut out_file = std::fs::OpenOptions::new()
        .create_new(true)
        .append(false)
        .write(true)
        .open(&temp_filename)
        .map_err(|e| IoError::LoadError {
            filename: temp_filename.to_owned(),
            source: e,
        })?;

    let mut prev_line: Option<String> = None;

    for line in reader.lines() {
        let line = line.map_err(|e| IoError::LoadError {
            filename: filename.to_string(),
            source: e,
        })?;

        if let Some(pl) = &prev_line {
            writeln!(&mut out_file, "{}", pl).map_err(|e| {
                IoError::WriteError {
                    filename: temp_filename.to_owned(),
                    source: e,
                }
            })?;
        }
        prev_line.replace(line);
    }

    if let Some(ll) = prev_line {
        std::fs::rename(temp_filename, filename).map_err(|e| {
            IoError::GenericError {
                message: format!(
                    "rename {} to {} error",
                    temp_filename, filename
                ),
                source: e,
            }
        })?;
        Ok(ll)
    } else {
        Err(IoError::OtherError("empty file".into()))
    }
}
