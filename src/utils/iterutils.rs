//
// iterutils.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

/// Stop iteration on error.
/// https://stackoverflow.com/a/63120052
/// Usage:
/// ```
/// let mut err = Ok(());
/// iter
///     ...
///     .scan(&mut err, until_err).
///     ...;
/// err?;
/// ```
pub fn until_err<T, E>(
    err: &mut &mut Result<(), E>,
    item: Result<T, E>,
) -> Option<T> {
    match item {
        Ok(item) => Some(item),
        Err(e) => {
            **err = Err(e);
            None
        }
    }
}
