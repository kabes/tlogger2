//
// mod.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use serde_derive::Deserialize;
use std::fs;
use std::io::prelude::*;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ConfError {
    #[error("failed to find user config directory")]
    MissingConfigDir,

    #[error("load config file {name} error: {err}")]
    IOError { name: String, err: String },

    #[error("parse config file error: {0}")]
    ParseError(String),
}

impl From<toml::de::Error> for ConfError {
    fn from(err: toml::de::Error) -> Self {
        ConfError::ParseError(err.to_string())
    }
}

fn get_config_path() -> Result<Option<PathBuf>, ConfError> {
    let mut cd = dirs_next::config_dir().ok_or(ConfError::MissingConfigDir)?;
    cd.push("tlogger2");
    cd.push("config.toml");

    Ok(if cd.exists() { Some(cd) } else { None })
}

#[derive(Deserialize, Debug)]
pub struct Config {
    #[serde(default = "Config::default_tlogger_file")]
    pub tlogger_file: String,

    #[serde(default = "Config::default_pomodoro_finish_cmd")]
    pub pomodoro_finish_cmd: String,

    #[serde(default = "Config::default_pomodoro_duration")]
    pub pomodoro_default_duration: String,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            pomodoro_finish_cmd: Config::default_pomodoro_finish_cmd(),
            pomodoro_default_duration: Config::default_pomodoro_duration(),
            tlogger_file: Config::default_tlogger_file(),
        }
    }
}

impl Config {
    fn default_tlogger_file() -> String {
        String::from("tlogger.txt")
    }
    fn default_pomodoro_finish_cmd() -> String {
        String::from("notify-send -u critical -i notification-message-im 'Pomodoro: Time run out'")
    }

    fn default_pomodoro_duration() -> String {
        String::from("0:15:00")
    }

    /// Load configuration from default path.
    pub fn load(path: Option<&str>) -> Result<Self, ConfError> {
        let path = if let Some(p) = path {
            Some(PathBuf::from(p))
        } else {
            get_config_path()?
        };

        if path.is_none() {
            return Ok(Config::default());
        }

        let path = path.unwrap();
        debug!("loading config file {:?}", &path);

        fs::File::open(&path)
            .map_err(|err| ConfError::IOError {
                name: path.to_str().unwrap().into(),
                err: err.to_string(),
            })
            .and_then(|mut file| {
                let mut contents = String::new();
                file.read_to_string(&mut contents)
                    .map_err(|err| ConfError::IOError {
                        name: path.to_str().unwrap().into(),
                        err: err.to_string(),
                    })
                    .and_then(|_| {
                        toml::from_str(&contents).map_err(ConfError::from)
                    })
            })
    }
}
