//
// time.rs
// Copyright (C) 2020 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use std::fmt::Write;

pub trait HumanFormat {
    fn to_human(&self) -> String;
}

impl HumanFormat for u32 {
    /// Format duration in sec to human representation.
    fn to_human(&self) -> String {
        let duration = self;
        let mut buf = String::new();
        let days = (duration / 86400) as u32;
        let hours = ((duration % 86400) / 3600) as u32;
        let minutes = ((duration % 3600) / 60) as u32;
        let seconds = (duration % 60) as u32;
        if days > 0 {
            buf.write_fmt(format_args!(
                "{}d {}:{:02}:{:02}",
                days, hours, minutes, seconds
            ))
            .expect("a Display implementation returned an error unexpectedly");
        } else if hours > 0 {
            buf.write_fmt(format_args!(
                "{}:{:02}:{:02}",
                hours, minutes, seconds
            ))
            .expect("a Display implementation returned an error unexpectedly");
        } else {
            buf.write_fmt(format_args!("{}:{:02}", minutes, seconds))
                .expect(
                    "a Display implementation returned an error unexpectedly",
                );
        }
        buf.shrink_to_fit();
        buf
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_format_duration() {
        assert_eq!(0u32.to_human(), String::from("0:00"));
        assert_eq!(1u32.to_human(), String::from("0:01"));
        assert_eq!(59u32.to_human(), String::from("0:59"));
        assert_eq!(60u32.to_human(), String::from("1:00"));
        assert_eq!(61u32.to_human(), String::from("1:01"));
        assert_eq!(181u32.to_human(), String::from("3:01"));
        assert_eq!(753u32.to_human(), String::from("12:33"));
        assert_eq!(3599u32.to_human(), String::from("59:59"));
        assert_eq!(3600u32.to_human(), String::from("1:00:00"));
        assert_eq!(3601u32.to_human(), String::from("1:00:01"));
        assert_eq!(5591u32.to_human(), String::from("1:33:11"));
        assert_eq!(49500u32.to_human(), String::from("13:45:00"));
        assert_eq!(86400u32.to_human(), String::from("1d 0:00:00"));
        assert_eq!(105093u32.to_human(), String::from("1d 5:11:33"));
        assert_eq!(299159u32.to_human(), String::from("3d 11:05:59"));
    }
}
