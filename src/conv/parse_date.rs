//
// parse_date.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kntbk>
// Distributed under terms of the GPLv3 license.
//

use chrono::{Datelike, Duration, NaiveDate, NaiveDateTime, NaiveTime};
use chronoutil::RelativeDuration;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ArgumentError {
    #[error("invalid range name: '{0}'")]
    InvalidName(String),

    #[error("invalid date/time format: '{0}'")]
    InvalidFormat(String),
}

fn date_quarter(date: NaiveDate) -> u8 {
    let month = date.month() + 2;
    (month / 3) as u8
}

fn quarter_start(year: i32, quarter: u8) -> NaiveDate {
    NaiveDate::from_ymd(year, (quarter * 3 - 2) as u32, 1)
}

fn day_offset(offset: Duration, today: NaiveDate) -> NaiveDateTime {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    date + offset
}

fn day_reloffset(offset: RelativeDuration, today: NaiveDate) -> NaiveDateTime {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    date + offset
}

fn prev_week(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let end =
        date - Duration::days(date.weekday().num_days_from_monday() as i64);
    let start = end - Duration::days(7);
    (start, end)
}

fn prev_month(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let end = date.with_day(1).unwrap();
    let start = end - Duration::days(1);
    let start = start.with_day(1).unwrap();
    (start, end)
}

fn prev_quarter(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let quarter = date_quarter(today);
    let (year, quarter) = if quarter == 1 {
        (today.year() - 1, 4)
    } else {
        (today.year(), quarter - 1)
    };
    let start = NaiveDateTime::new(
        quarter_start(year, quarter),
        NaiveTime::from_hms(0, 0, 0),
    );
    let end = start + RelativeDuration::months(3);
    (start, end)
}

fn prev_year(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let end = date.with_day(1).unwrap().with_month(1).unwrap();
    let start = end - Duration::days(1);
    let start = start.with_day(1).unwrap().with_month(1).unwrap();
    (start, end)
}

fn this_week(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let start =
        date - Duration::days(date.weekday().num_days_from_monday() as i64);
    let end = start + Duration::days(7);
    (start, end)
}

fn this_month(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let start = date.with_day(1).unwrap();
    let end = start + Duration::days(32);
    let end = end.with_day(1).unwrap();
    (start, end)
}

fn this_quarter(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let quarter = date_quarter(today);
    let start = NaiveDateTime::new(
        quarter_start(today.year(), quarter),
        NaiveTime::from_hms(0, 0, 0),
    );
    let end = start + RelativeDuration::months(3);
    (start, end)
}

fn this_year(today: NaiveDate) -> (NaiveDateTime, NaiveDateTime) {
    let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
    let start = date.with_day(1).unwrap().with_month(1).unwrap();
    let end = start.with_year(start.year() + 1).unwrap();
    (start, end)
}

pub fn parse_range_name(
    name: &str,
    today: Option<NaiveDate>,
) -> Result<(NaiveDateTime, NaiveDateTime), ArgumentError> {
    let today =
        today.unwrap_or_else(|| chrono::Local::now().naive_local().date());
    let result = match name {
        "today" => (
            day_offset(Duration::days(0), today),
            day_offset(Duration::days(1), today),
        ),
        "yesterday" => (
            day_offset(Duration::days(-1), today),
            day_offset(Duration::days(0), today),
        ),
        "past-week" => (
            day_offset(Duration::days(-7), today),
            day_offset(Duration::days(1), today),
        ),
        "past-month" => (
            day_reloffset(RelativeDuration::months(-1), today),
            day_offset(Duration::days(1), today),
        ),
        "past-year" => {
            let date = NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0));
            let date = date.with_year(date.year() - 1).unwrap();
            (date, day_offset(Duration::days(1), today))
        }
        "prev-week" => prev_week(today),
        "prev-month" => prev_month(today),
        "prev-quarter" => prev_quarter(today),
        "prev-year" => prev_year(today),
        "this-week" => this_week(today),
        "this-month" => this_month(today),
        "this-quarter" => this_quarter(today),
        "this-year" => this_year(today),
        _ => {
            return Err(ArgumentError::InvalidName(name.to_string()));
        }
    };
    debug!("parse range {} to {:?} - {:?}", name, result.0, result.1);
    Ok(result)
}

pub fn parse_date_str(
    date: &str,
    end: bool,
) -> Result<NaiveDateTime, ArgumentError> {
    let date_len = date.len();
    let date = date.replace(" ", "T");
    let date = match date_len {
        // full date
        19 => date,
        16 => date + ":00",
        13 => date + ":00:00",
        10 => date + "T00:00:00",
        7 => date + "-01T00:00:00",
        4 => date + "-01-01T00:00:00",
        _ => return Err(ArgumentError::InvalidFormat(date)),
    };
    debug!("date {:?}, {}", date, end);
    let ndate = NaiveDateTime::parse_from_str(&date, "%Y-%m-%dT%H:%M:%S")
        .map_err(|_| ArgumentError::InvalidFormat(date.to_string()))?;

    if !end {
        return Ok(ndate);
    }

    let ndate = match date_len {
        19 => ndate,
        16 => ndate + Duration::minutes(1),
        13 => ndate + Duration::hours(1),
        10 => ndate + Duration::days(1),
        // month
        7 => ndate + RelativeDuration::months(1),
        4 => ndate + RelativeDuration::years(1),
        _ => unreachable!(),
    };

    debug!("date {:?}, {} -> {:?}", date, end, ndate);

    Ok(ndate)
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

    fn mkdt(y: i32, m: u32, d: u32) -> NaiveDateTime {
        NaiveDateTime::new(
            NaiveDate::from_ymd(y, m, d),
            NaiveTime::from_hms(0, 0, 0),
        )
    }

    #[test]
    fn test_parse_prev_week() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(prev_week(now), (mkdt(2020, 2, 24), mkdt(2020, 3, 2)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(prev_week(now), (mkdt(2020, 12, 21), mkdt(2020, 12, 28)));
    }

    #[test]
    fn test_parse_prev_month() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(prev_month(now), (mkdt(2020, 2, 1), mkdt(2020, 3, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(prev_month(now), (mkdt(2020, 12, 1), mkdt(2021, 1, 1)));
    }

    #[test]
    fn test_parse_prev_year() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(prev_year(now), (mkdt(2019, 1, 1), mkdt(2020, 1, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(prev_year(now), (mkdt(2020, 1, 1), mkdt(2021, 1, 1)));
    }

    #[test]
    fn test_parse_this_week() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(this_week(now), (mkdt(2020, 3, 2), mkdt(2020, 3, 9)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(this_week(now), (mkdt(2020, 12, 28), mkdt(2021, 1, 4)));
    }

    #[test]
    fn test_parse_this_month() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(this_month(now), (mkdt(2020, 3, 1), mkdt(2020, 4, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(this_month(now), (mkdt(2021, 1, 1), mkdt(2021, 2, 1)));
    }

    #[test]
    fn test_parse_this_year() {
        let now = NaiveDate::from_ymd(2020, 3, 5);
        assert_eq!(this_year(now), (mkdt(2020, 1, 1), mkdt(2021, 1, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(this_year(now), (mkdt(2021, 1, 1), mkdt(2022, 1, 1)));
    }

    #[test]
    fn test_parse_this_quarter() {
        let now = NaiveDate::from_ymd(2020, 4, 5);
        assert_eq!(this_quarter(now), (mkdt(2020, 4, 1), mkdt(2020, 7, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(this_quarter(now), (mkdt(2021, 1, 1), mkdt(2021, 4, 1)));
    }

    #[test]
    fn test_parse_prev_quarter() {
        let now = NaiveDate::from_ymd(2020, 4, 5);
        assert_eq!(prev_quarter(now), (mkdt(2020, 1, 1), mkdt(2020, 4, 1)));
        let now = NaiveDate::from_ymd(2021, 1, 2);
        assert_eq!(prev_quarter(now), (mkdt(2020, 10, 1), mkdt(2021, 1, 1)));
    }
}
