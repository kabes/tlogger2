Tlogger2 2.x
============

Log time spent on various activities/tasks/events.

Insipred by: "timeflow" [https://github.com/trimailov/timeflow] and "gtimelog" [http://gtimelog.org/] (same file format).

Events are stored in text file, in format: `<date>: <message> [# <comment>]`.

Date format is `yyyy-mm-dd hh:mm` or `yyyy-mm-dd hh:mm:ss` (24-hours).

Message my starts with "project name" separated from rest message by `: `, i.e: `project1: rest of message`.
Project may have subproject (separated by `:`), i.e.: `project1:task1:`

If message ends with `**` is "marker" - virtual event that is not reported but mark point in time when next entry started.

If message ends with `*` are comments (my by reported)

Message may contain tags in form `key:value`. Special key is `count` which is summed in reports.

Lines that stats with `#` are ignored.


## Examples

Example records:
```
2018-01-01 12:23: start **
2018-01-01 13:24: coffee *
2018-01-01 14:00: finish task1
2018-01-02 13:23: start **
2018-01-02 13:24: # note about something
2018-01-02 13:30: proj1: finish task2
2018-01-02 13:32: coffee count:2 *
2018-01-02 13:50: proj1: finish task3 # done part 1
2018-01-02 14:00: # another note
```

Example, basic report (`tlogger2 report`):
```
Title                                                              Duration   Count   Entr.
==================================================================================================
# another note                                                        11:00       1       1
# note about something                                                 2:00       1       1
finish task1                                                        1:38:00       1       1
proj1: finish task2                                                    7:00       1       1
proj1: finish task3                                                   21:00       1       1
==================================================================================================
Total                                                               2:19:00       5       5
```

Report by projects (`tlogger2 report -g project`):
```
Title                                                              Duration   Count   Entr.
==================================================================================================
                                                                    1:51:00       3       3
proj1                                                                 28:00       2       2
==================================================================================================
Total                                                               2:19:00       5       5

```



# Running
```
    tlogger2 [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -v, --verbose    Sets the level of verbosity
    -V, --version    Prints version information

OPTIONS:
    -c, --config <config>    Configuration file name; default for Linux:
                             $XDG_CONFIG/tlogger2/config.yml or $HOME/.config/tlogger2/config.yml
                              [env: TLOGGER_CONF=]
    -f, --file <file>...     Set a custom log file. Last file is used for writing into.
                              [env: TLOGGER_FILE=]

SUBCOMMANDS:
    add         Add new entry to log file.
    archive     Move old entries into separate files.
    editor      Start editor and edit log file.
    end         Add entry with given duration (add also start marker).
    generate    Generate shell completion routines.
    help        Prints this message or the help of the given subcommand(s)
    note        Add a note (comment)
    pomodoro    Start pomodoro timer.
    print       Load & print log file content.
    projects    Show used projects (in all or filtered entries)
    report      Create report that shows time spend on each project, task, etc.
    start       Add `start` marker
    status      Print current status.
    undo        Remove and show last entry from log file.


```


Detailed help for each command: `tlogger2 <command> --help`


# Example workflow

1. log start working:

    `tlogger2 start`

1. log finished "task 1" in "project-1":

    `tlogger2 add project-1: task 1`

1. work on another task, and mark it finish:

    `tlogger2 add task2`

1. take a break

1. log start work again (manual method):

    `logger2 add start working after break**`

1. finish "task 1" again:

    `tlogger2 add project-1: task 1`

1. get report for today work:

    `tlogger2 status`
	`tlogger2 report -P today`
	`tlogger2 report -P today -g project`

1. add some note:

    `tlogger2 note this is note`



# Reports

Report show data for given period with optional grouping and filtering.

Period may be selected by (`-P`, `--period`):
* `today`
* `yesterday`
* `past-week`
* `past-month`
* `past-year`
* `prev-week`
* `prev-month`
* `prev-quarter`
* `prev-year`
* `this-week`
* `this-month`
* `this-quarter`
* `this-year`

Basic filtering (`-c`, `--contains`) select events that contain given world in `message` or `note`.

Entries may by grouped (`-g`, `--group`) by:
* `project` or `project<max-level>` (i.e. `project2`)
* time period: `year`, `month`, `quarter`, `week`, `day`

Grouping may be limited to given level (`--max-depth`).

## Examples
* `tlogger2 report range:prev-month`
* `tlogger2 report -g project2 range:prev-week`
* `tlogger2 report -g project,month`



# Filtering

Commands like `print` or  `report` allow use advanced filter to select data.
Definition:

```
Filters: <expr>

expr := <expr> or|and <expr>
expr := not <expr> - negate expression
expr := ( <expr> ) - group expression

expr := proj:<name> - filter by project name part
expr := tag:<tag name> - filter by tag
expr := begin:<date|date time> - filter by dates not before given
expr := end:<date|date time> - filter by dates not after given
expr := type:note|marger|task|counter - filter by entry type
expr := range:this-year|last-month... - filter by predefined periods
expr := <string> - filter by message
```

## Examples
* `"range:last-month and (proj:project1 or proj:project2)"`
* `"begin:2021 and end:2021-02 and not tag:unknown and testing"`



# Configuration file

Some parameters may be configured globally in `config.toml` file located in
`$XDG_CONFIG/tlogger2/config.yml` or `$HOME/.config/tlogger2/config.yml` file.
See `sample_config.toml` for example.


# Licence

Copyright (c) Karol Będkowski, 2021

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For details please see COPYING file.
